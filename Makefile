CC = psp-g++
INCDIRS = -I$(PSPDEV)/psp/include -I$(PSPDEV)/psp/sdk/include -I.
LIBDIRS = -L$(PSPDEV)/lib -L$(PSPDEV)/psp/lib -L$(PSPDEV)/psp/sdk/lib
LIBS = -lpspdebug -lpspdisplay -lpspge -lpspgum_vfpu -lpspvfpu -lpspgu -lpspctrl -lpspaudio -lpspaudiolib
CFLAGS = -DPSP_BUILD -O2 -G0
LDFLAGS = -Wl,-zmax-page-size=128

TARGET = GameMathTest

SOURCES = test/psp/pspTest.cpp

all:
	$(CC) $(INCDIRS) $(CFLAGS) $(LIBDIRS) $(LDFLAGS) $(SOURCES) -o $(TARGET) $(LIBS)
	psp-fixup-imports ./$(TARGET)
	mksfoex -d MEMSIZE=1 $(TARGET) PARAM.SFO
	pack-pbp EBOOT.PBP PARAM.SFO NULL NULL NULL NULL NULL $(TARGET) NULL

clean:
	rm -rf $(TARGET) *.elf *.SFO EBOOT.PBP
