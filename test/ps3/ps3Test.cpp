// psl1ght math
#include <vectormath/cpp/vectormath_aos.h>

#include <ps3/GameMath.h>
#include <cstdio>


using namespace GameMath;

#if 1
static void printMat4(const Mat4& m)
{
    const float* p = (const float*)&m;
    printf(
        "%f, %f, %f, %f\n"
        "%f, %f, %f, %f\n"
        "%f, %f, %f, %f\n"
        "%f, %f, %f, %f\n",
        p[0], p[4], p[8], p[12],
        p[1], p[5], p[9], p[13],
        p[2], p[6], p[10], p[14],
        p[3], p[7], p[11], p[15]);
}

static void printQuat(const Quat& q)
{
    printf("%f, %f, %f, %f\n", q.x, q.y, q.z, q.w);
}
#endif

#define EPSILON 0.0001f
static inline bool floatsEqual(const float a, const float b)
{
    return (fabsf(a - b) < EPSILON);
}
static inline bool vec3sEqual(const Vec3& a, const Vec3& b)
{
    return (floatsEqual(a.x, b.x) &&
        floatsEqual(a.y, b.y) &&
        floatsEqual(a.z, b.z));
}
static inline bool vec4sEqual(const Vec4& a, const Vec4& b)
{
    return (floatsEqual(a.x, b.x) &&
        floatsEqual(a.y, b.y) &&
        floatsEqual(a.z, b.z) &&
        floatsEqual(a.w, b.w));
}
static inline bool mat4sEqual(const Mat4& a, const Mat4& b)
{
    return (vec4sEqual(*((Vec4*)a.v[0]), *((Vec4*)b.v[0])) &&
        vec4sEqual(*((Vec4*)a.v[1]), *((Vec4*)b.v[1])) &&
        vec4sEqual(*((Vec4*)a.v[2]), *((Vec4*)b.v[2])) &&
        vec4sEqual(*((Vec4*)a.v[3]), *((Vec4*)b.v[3])));
}

static inline void printVec4(const Vec4& v)
{
    printf("%f,%f,%f,%f\n", v.x, v.y, v.z, v.w);
}

static bool testVec3Add()
{
    Vec3 a(1.0f, 2.0f, 3.0f);
    Vec3 b(3.0f, 4.0f, 5.0f);

    Vec3 c = a + b;

    Vec3 expected(4.0f, 6.0f, 8.0f);
    return vec3sEqual(c, expected);
}

static bool testVec3Sub()
{
    Vec3 a(1.0f, 2.0f, 3.0f);
    Vec3 b(3.0f, 2.0f, 1.0f);

    Vec3 c = a - b;

    Vec3 expected(-2.0f, 0.0f, 2.0f);
    return vec3sEqual(c, expected);
}

static bool testVec3PlusEquals()
{
    Vec3 a(1.0f, 2.0f, 3.0f);
    Vec3 b(1.0f, 1.0f, 1.0f);

    a += b;

    Vec3 expected(2.0f, 3.0f, 4.0f);
    return vec3sEqual(expected, a);
}

static bool testVec3MinusEquals()
{
    Vec3 a(1.0f, 2.0f, 3.0f);
    Vec3 b(1.0f, 1.0f, 1.0f);

    a -= b;

    Vec3 expected(0.0f, 1.0f, 2.0f);
    return vec3sEqual(expected, a);
}

static bool testVec3TimesScale()
{
    Vec3 a(2.0f, 3.0f, 4.0f);
    float scale = 2.0f;
    
    a = a * scale;
    
    Vec3 expected(4.0f, 6.0f, 8.0f);
    return vec3sEqual(expected, a);
}

static bool testVec3Dot()
{
    Vec3 a(1.0f, 2.0f, 3.0f);
    Vec3 b(4.0f, 5.0f, 6.0f);
    float res = Dot(a, b);
    return floatsEqual(res, 32.0f);
}

static bool testVec3Cross()
{
    Vec3 a(1.0f, 2.0f, 3.0f);
    Vec3 b(4.0f, 5.0f, 6.0f);
    Vec3 res = Cross(a, b);
    Vec3 expected(-3.0f, 6.0f, -3.0f);
    return vec3sEqual(res, expected);
}

static bool testVec3Normalize()
{
    Vec3 a(1.0f, 2.0f, 3.0f);
    Vec3 res = Normalize(a);
    Vec3 expected(0.267261f, 0.534522f, 0.801784f);
    return vec3sEqual(res, expected);
}

static bool testMat4DefaultConstructor()
{
    Mat4 m;

    bool res = true;

    res &= floatsEqual(m(0,0), 1.0f);
    res &= floatsEqual(m(1,1), 1.0f);
    res &= floatsEqual(m(2,2), 1.0f);
    res &= floatsEqual(m(3,3), 1.0f);

    res &= floatsEqual(m(0,1), 0.0f);
    res &= floatsEqual(m(0,2), 0.0f);
    res &= floatsEqual(m(0,3), 0.0f);

    res &= floatsEqual(m(1,0), 0.0f);
    res &= floatsEqual(m(1,2), 0.0f);
    res &= floatsEqual(m(1,3), 0.0f);

    res &= floatsEqual(m(2,0), 0.0f);
    res &= floatsEqual(m(2,1), 0.0f);
    res &= floatsEqual(m(2,3), 0.0f);

    res &= floatsEqual(m(3,0), 0.0f);
    res &= floatsEqual(m(3,1), 0.0f);
    res &= floatsEqual(m(3,2), 0.0f);

    return res;
}

static bool testMat4CopyConstructor()
{
    Mat4 m1(
        1.0f, 2.0f, 3.0f, 4.0f,
        5.0f, 6.0f, 7.0f, 8.0f,
        9.0f, 10.0f, 11.0f, 12.0f,
        13.0f, 14.0f, 15.0f, 16.0f);

    Mat4 m2(m1);

    bool res = true;
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            res &= floatsEqual(m1(i,j), m2(i,j));
        }
    }

    return res;
}

static bool testMat4Translate()
{
    Vec3 trans(1.0f, 2.0f, 3.0f);
    Mat4 m = GameMath::Translate(trans);

    bool res = true;
    res &= floatsEqual(m(0,3), trans.x);
    res &= floatsEqual(m(1,3), trans.y);
    res &= floatsEqual(m(2,3), trans.z);

    return res;
}

static bool testMat4TranslateVec3()
{
    Vec3 v(1.0f, 2.0f, 3.0f);
    Vec3 trans(1.0f, 2.0f, 3.0f);

    Mat4 m = GameMath::Translate(trans);
    Vec3 v2 = GameMath::Transform(m, v);

    Vec3 expected(2.0f, 4.0f, 6.0f);
    return vec3sEqual(expected, v2);
}

static bool testMat4RotateVec3()
{
    // rotate right-pointing vector about z axis to point upwards
    Mat4 m = GameMath::Rotate(GameMath::Deg2Rad(90.0f), Vec3(0.0f, 0.0f, 1.0f));
    Vec3 v(1.0f, 0.0f, 0.0f);
    v = GameMath::Transform(m, v);
    Vec3 expected(0.0f, 1.0f, 0.0f);
    bool result = vec3sEqual(expected, v);

    // rotate right-pointing vector about y axis to point forwards
    m = GameMath::Rotate(GameMath::Deg2Rad(90.0f), Vec3(0.0f, 1.0f, 0.0f));
    v = Vec3(1.0f, 0.0f, 0.0f);
    v = GameMath::Transform(m, v);
    expected = Vec3(0.0f, 0.0f, -1.0f);
    result &= vec3sEqual(expected, v);

    // rotate up-pointing vector about x axis to point backwards
    m = GameMath::Rotate(GameMath::Deg2Rad(90.0f), Vec3(1.0f, 0.0f, 0.0f));
    v = Vec3(0.0f, 1.0f, 0.0f);
    v = GameMath::Transform(m, v);
    expected = Vec3(0.0f, 0.0f, 1.0f);
    result &= vec3sEqual(expected, v);

    return result;
}

static bool testMat4ScaleVec3()
{
    Mat4 m = GameMath::Scale(Vec3(2.0f, 3.0f, 4.0f));
    Vec3 v(2.0f, 3.0f, 4.0f);
    v = GameMath::Transform(m, v);
    
    Vec3 expected(4.0f, 9.0f, 16.0f);
    return vec3sEqual(expected, v);
}

static bool testMat4MulVec4()
{
    Mat4 m = GameMath::Translate(1.0f, 2.0f, 3.0f);
    Vec4 v(4.0f, 5.0f, 6.0f, 1.0f);
    Vec4 res = m * v;
    Vec4 expected(5.0f, 7.0f, 9.0f, 1.0f);
    return vec4sEqual(expected, res);
}

static bool testMat4MulScalar()
{
    Mat4 m(1.0f, 2.0f, 3.0f, 4.0f,
           5.0f, 6.0f, 7.0f, 8.0f,
           9.0f, 10.0f, 11.0f, 12.0f,
           13.0f, 14.0f, 15.0f, 16.0f);
    Mat4 res = m * 0.5f;
    Mat4 expected(0.5f, 1.0f, 1.5f, 2.0f,
                    2.5f, 3.0f, 3.5f, 4.0f,
                    4.5f, 5.0f, 5.5f, 6.0f,
                    6.5f, 7.0f, 7.5f, 8.0f);
    return mat4sEqual(res, expected);
}

static bool testMat4Add()
{
    Mat4 m1(1.0f, 2.0f, 3.0f, 4.0f,
           5.0f, 6.0f, 7.0f, 8.0f,
           9.0f, 10.0f, 11.0f, 12.0f,
           13.0f, 14.0f, 15.0f, 16.0f);
    Mat4 m2(m1);
    Mat4 res = m1 + m2;
    Mat4 expected(2.0f, 4.0f, 6.0f, 8.0f,
                10.0f, 12.0f, 14.0f, 16.0f,
                18.0f, 20.0f, 22.0f, 24.0f,
                26.0f, 28.0f, 30.0f, 32.0f);
    return mat4sEqual(res, expected);
}

#if 0
static void printMat4(const Mat4& m)
{
    const float* p = (const float*)&m;
    printf(
        "%f, %f, %f, %f\n"
        "%f, %f, %f, %f\n"
        "%f, %f, %f, %f\n"
        "%f, %f, %f, %f\n",
        p[0], p[4], p[8], p[12],
        p[1], p[5], p[9], p[13],
        p[2], p[6], p[10], p[14],
        p[3], p[7], p[11], p[15]);
}

static void printQuat(const Quat& q)
{
    printf("%f, %f, %f, %f\n", q.x, q.y, q.z, q.w);
}
#endif

// scale rotate translate matrix (TRS since Left-to-Right multiplication)
static bool testMat4SRTMat()
{
    Quat rotQuat(0.259f, 0.0f, 0.0f, 0.966f); // x,y,z,w
    Mat4 translateMat = GameMath::Translate(10.0f, 20.0f, 30.0f);
    Mat4 rotateMat = GameMath::QuatToMat4(rotQuat);
    Mat4 scaleMat = GameMath::Scale(2.0f, 1.0f, 0.5f);

    Mat4 m =
        translateMat *
        rotateMat *
        scaleMat;
    
#if 0
    printf("translateMat:\n");
    printMat4(translateMat);
    printf("rotateMat:\n");
    printMat4(rotateMat);
    printf("scaleMat:\n");
    printMat4(scaleMat);

    printf("transformMat:\n");
    printMat4(m);
#endif

    // generated via glm
    Mat4 expected(
        2.000000f, 0.000000f, 0.000000f, 10.000000f,
        0.000000f, 0.865838f, -0.250194f, 20.000000f,
        0.000000f, 0.500388f, 0.432919f, 30.000000f,
        0.000000f, 0.000000f, 0.000000f, 1.000000f
    );

    bool result = true;
    for (int row = 0; row < 4; ++row)
    {
        for (int col = 0; col < 4; ++col)
        {
            result &= floatsEqual(expected(row,col), m(row,col));
        }
    }

    return result;
}

static bool testMat4Inverse()
{
    // generated via glm
    Mat4 m(
        2.000000f, 0.000000f, 0.000000f, 10.000000f,
        0.000000f, 0.865838f, -0.250194f, 20.000000f,
        0.000000f, 0.500388f, 0.432919f, 30.000000f,
        0.000000f, 0.000000f, 0.000000f, 1.000000f
    );

    Mat4 inverted = GameMath::Inverse(m);

    Mat4 expected(
        0.500000f, -0.000000f, 0.000000f, -5.000000f,
        -0.000000f, 0.865783f, 0.500356f, -32.326344f,
        0.000000f, -1.000712f, 1.731566f, -31.932724f,
        -0.000000f, 0.000000f, -0.000000f, 1.000000f
    );
    bool result = true;
    for (int row = 0; row < 4; ++row)
    {
        for (int col = 0; col < 4; ++col)
        {
            result &= floatsEqual(expected(row,col), inverted(row,col));
        }
    }

    return result;
}

static bool testMat4Transpose()
{
    // generated via glm
    Mat4 m(
        2.000000f, 0.000000f, 0.000000f, 10.000000f,
        0.000000f, 0.865838f, -0.250194f, 20.000000f,
        0.000000f, 0.500388f, 0.432919f, 30.000000f,
        0.000000f, 0.000000f, 0.000000f, 1.000000f
    );

    Mat4 tpose = GameMath::Transpose(m);

    Mat4 expected(
        2.000000f, 0.000000f, 0.000000f, 0.000000f,
        0.000000f, 0.865838f, 0.500388f, 0.000000f,
        0.000000f, -0.250194f, 0.432919f, 0.000000f,
        10.000000f, 20.000000f, 30.000000f, 1.000000f
    );
    bool result = true;
    for (int row = 0; row < 4; ++row)
    {
        for (int col = 0; col < 4; ++col)
        {
            result &= floatsEqual(expected(row,col), tpose(row,col));
        }
    }

    return result;
}

static bool testMat4Perspective()
{
    Mat4 m = GameMath::Perspective(
        45.0f * M_PI / 180.0f,
        1024.0f/768.0f,
        1.0f,
        1000.0f
    );
    Vectormath::Aos::Matrix4 expected = 
        Vectormath::Aos::Matrix4::perspective(
            45.0f * M_PI / 180.0f,
            1024.0f/768.0f,
            1.0f,
            1000.0f );
    float* a = (float*)&m;
    float* b = (float*)&expected;
    bool result = true;
    for (int i=0; i<16; ++i) {
        result &= floatsEqual(a[i], b[i]);
    }
    return result;
}

// https://www.omnicalculator.com/math/quaternion
static bool testQuatMul()
{
    Quat q1(1.0f, 2.0f, 3.0f, 4.0f);
    Quat q2(5.0f, 6.0f, 7.0f, 8.0f);

    Quat q = q1 * q2;
    Quat expected(24.0f, 48.0f, 48.0f, -6.0f);

    return (floatsEqual(q.x, expected.x) &&
        floatsEqual(q.y, expected.y) &&
        floatsEqual(q.z, expected.z) &&
        floatsEqual(q.w, expected.w));
}

static bool testQuatRot()
{
    // rotate right-pointing vector about z axis to point upwards
    Quat q(GameMath::Deg2Rad(90.0f), Vec3(0.0f, 0.0f, 1.0f));
    Vec3 v(1.0f, 0.0f, 0.0f);
    v = GameMath::Transform(q, v);
    Vec3 expected(0.0f, 1.0f, 0.0f);
    bool result = vec3sEqual(expected, v);

    // rotate right-pointing vector about y axis to point forwards
    q = Quat(GameMath::Deg2Rad(90.0f), Vec3(0.0f, 1.0f, 0.0f));
    v = Vec3(1.0f, 0.0f, 0.0f);
    v = GameMath::Transform(q, v);
    expected = Vec3(0.0f, 0.0f, -1.0f);
    result &= vec3sEqual(expected, v);

    // rotate up-pointing vector about x axis to point backwards
    q = Quat(GameMath::Deg2Rad(90.0f), Vec3(1.0f, 0.0f, 0.0f));
    v = Vec3(0.0f, 1.0f, 0.0f);
    v = GameMath::Transform(q, v);
    expected = Vec3(0.0f, 0.0f, 1.0f);
    result &= vec3sEqual(expected, v);

    return result;
}

static bool testQuatNormalize()
{
    Quat q(1.0f, 2.0f, 3.0f, 4.0f); 
    q = GameMath::Normalize(q);
    float len = GameMath::Mag(q);
    return floatsEqual(len, 1.0f);
}

static bool testQuatToMat4()
{
    Quat q(1.0f, 2.0f, 3.0f, 4.0f);
    Mat4 m = GameMath::QuatToMat4(q);
    Mat4 expected(
        -25.000000, -20.000000, 22.000000, 0.000000,
        28.000000, -19.000000, 4.000000, 0.000000,
        -10.000000, 20.000000, -9.000000, 0.000000,
        0.000000, 0.000000, 0.000000, 1.000000
    );
    //printf("QuatToMat4 expected:\n");
    //printMat4(expected);
    //printf("QuatToMat4 actual:\n");
    //printMat4(m);
    return mat4sEqual(m, expected);
}

int main()
{
    int passedCount = 0;
    int numTests = 0;

#define RunTest(result, name) \
    ++numTests; \
    if (!result) { printf("Test %s failed\n", name); } \
    else { printf("Test %s passed\n", name); ++passedCount; }

    RunTest(testVec3Add(), "testVec3Add")
    RunTest(testVec3Sub(), "testVec3Sub")
    RunTest(testVec3PlusEquals(), "testVec3PlusEquals")
    RunTest(testVec3MinusEquals(), "testVec3MinusEquals")
    RunTest(testVec3TimesScale(), "testVec3TimesScale")
    RunTest(testVec3Dot(), "testVec3Dot")
    RunTest(testVec3Cross(), "testVec3Cross")
    RunTest(testVec3Normalize(), "testVec3Normalize")

    RunTest(testMat4DefaultConstructor(), "testMat4DefaultConstructor")
    RunTest(testMat4CopyConstructor(), "testMat4CopyConstructor")
    RunTest(testMat4Translate(), "testMat4Translate")
    RunTest(testMat4TranslateVec3(), "testMat4TranslateVec3")
    RunTest(testMat4RotateVec3(), "testMat4RotateVec3")
    RunTest(testMat4ScaleVec3(), "testMat4ScaleVec3")
    RunTest(testMat4SRTMat(), "testMat4SRTMat")
    RunTest(testMat4Inverse(), "testMat4Inverse")
    RunTest(testMat4Transpose(), "testMat4Transpose")
    RunTest(testMat4MulVec4(), "testMat4MulVec4")
    RunTest(testMat4MulScalar(), "testMat4MulScalar")
    RunTest(testMat4Add(), "testMat4Add")
    RunTest(testMat4Perspective(), "testMat4Perspective")

    RunTest(testQuatMul(), "testQuatMul")
    RunTest(testQuatRot(), "testQuatRot")
    RunTest(testQuatNormalize(), "testQuatNormalize")
    RunTest(testQuatToMat4(), "testQuatToMat4")

    printf("%d/%d tests passed\n", passedCount, numTests);

    return 0;
}

