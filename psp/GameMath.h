#ifndef GAMEMATH_CPP_H_INCLUDED
#define GAMEMATH_CPP_H_INCLUDED

#include <cmath>
#include <cstring>

#include <pspgum.h>
#include <pspvfpu.h>

#ifndef M_PI
#define M_PI 3.141592653589793f
#endif

// https://github.com/pspdev/pspsdk/blob/master/src/gum/pspgum_vfpu.c
extern struct pspvfpu_context* gum_vfpucontext;

namespace GameMath
{

struct Vec2
{
    union
    {
        struct { float x, y; };
        struct { float u, v; };
    };

    Vec2() :
        x(0.0f),
        y(0.0f)
    {}

    Vec2(float x, float y) :
        x(x),
        y(y)
    {}

    Vec2(const Vec2& other) :
        x(other.x),
        y(other.y)
    {}

    Vec2& operator=(const Vec2& other)
    {
        x = other.x;
        y = other.y;
        return *this;
    }

    Vec2& operator+=(const Vec2& other)
    {
        x += other.x;
        y += other.y;
        return *this;
    }

    Vec2& operator-=(const Vec2& other)
    {
        x -= other.x;
        y -= other.y;
        return *this;
    }
};

inline Vec2 operator+(const Vec2& lhs, const Vec2& rhs) {
    return Vec2(
        lhs.x+rhs.x,
        lhs.y+rhs.y
    );
}
inline Vec2 operator-(const Vec2& lhs, const Vec2& rhs) {
    return Vec2(
        lhs.x-rhs.x,
        lhs.y-rhs.y
    );
}
inline Vec2 operator*(const Vec2& lhs, const float rhs) {
    return Vec2(
        lhs.x * rhs,
        lhs.y * rhs
    );
}
inline Vec2 operator*(const float& lhs, const Vec2& rhs) {
    return rhs * lhs;
}
inline Vec2 Normalize(const Vec2& v) {
    const float mag = sqrtf(v.x*v.x + v.y*v.y);
    return Vec2(
        v.x / mag,
        v.y / mag
    );
}


struct Vec3
{
    union
    {
        struct { float x,y,z; };
        float v[3];
    };

    Vec3() :
        x(0.0f),
        y(0.0f),
        z(0.0f)
    {}

    Vec3(float x, float y, float z) :
        x(x),
        y(y),
        z(z)
    {}

    Vec3(const Vec3& other) :
        x(other.x),
        y(other.y),
        z(other.z)
    {}

    Vec3& operator=(const Vec3& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;
        
        return *this;
    }

    Vec3& operator+=(const Vec3& other) {
        x += other.x;
        y += other.y;
        z += other.z;

        return *this;
    }
    Vec3& operator-=(const Vec3& other) {
        x -= other.x;
        y -= other.y;
        z -= other.z;

        return *this;
    }
};

struct Vec4
{
    union
    {
        struct { float x,y,z,w; };
        float v[4];
    };

    Vec4() :
        x(0.0f),
        y(0.0f),
        z(0.0f),
        w(1.0f)
    {}

    Vec4(float x, float y, float z, float w) :
        x(x),
        y(y),
        z(z),
        w(w)
    {}

    Vec4(const Vec4& other) :
        x(other.x),
        y(other.y),
        z(other.z),
        w(other.w)
    {}

    Vec4(const Vec3& vec, const float w) :
        x(vec.x),
        y(vec.y),
        z(vec.z),
        w(w)
    {}

    Vec4& operator=(const Vec4& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;
        w = other.w;
        
        return *this;
    }

    Vec4& operator+=(const Vec4& other) {
        x += other.x;
        y += other.y;
        z += other.z;
        w += other.w;

        return *this;
    }
    Vec4& operator-=(const Vec4& other) {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        w -= other.w;

        return *this;
    }
};

inline Vec3 operator+(const Vec3& lhs, const Vec3& rhs) {
    return Vec3(
        lhs.x + rhs.x,
        lhs.y + rhs.y,
        lhs.z + rhs.z
    );
}
inline Vec3 operator-(const Vec3& lhs, const Vec3& rhs) {
    return Vec3(
        lhs.x - rhs.x,
        lhs.y - rhs.y,
        lhs.z - rhs.z
    );
}
inline Vec3 operator*(const Vec3& lhs, const Vec3& rhs) {
    return Vec3(
        lhs.x * rhs.x,
        lhs.y * rhs.y,
        lhs.z * rhs.z
    );
}
inline Vec3 operator*(const Vec3& v, const float scale) {
    return Vec3(
        v.x * scale,
        v.y * scale,
        v.z * scale
    );
}
inline Vec3 operator*(const float scale, const Vec3& v) {
    return Vec3(
        v.x * scale,
        v.y * scale,
        v.z * scale
    );
}
inline float Dot(const Vec3& lhs, const Vec3& rhs) {
    return (lhs.x*rhs.x + lhs.y*rhs.y + lhs.z*rhs.z);
}
inline Vec3 Cross(const Vec3& lhs, const Vec3& rhs) {
    return Vec3(
        lhs.y*rhs.z - lhs.z*rhs.y,
        lhs.z*rhs.x - lhs.x*rhs.z,
        lhs.x*rhs.y - lhs.y*rhs.x
    );
}
inline Vec3 Normalize(const Vec3& v) {
    const float magInv = 1.0f / sqrtf(v.x*v.x + v.y*v.y + v.z*v.z);
    return Vec3(
        v.x * magInv,
        v.y * magInv,
        v.z * magInv
    );
}


struct Quat
{
    union
    {
        struct { float x,y,z,w; };
        float v[4];
    };

    Quat() :
        x(0.0f),
        y(0.0f),
        z(0.0f),
        w(1.0f)
    {}

    Quat(float x, float y, float z, float w) :
        x(x),
        y(y),
        z(z),
        w(w)
    {}

    Quat(const float angleRadians, const Vec3& axis)
    {
        const float sinHalf = sinf(angleRadians * 0.5f);
        const float cosHalf = cosf(angleRadians * 0.5f);
        x = axis.x * sinHalf;
        y = axis.y * sinHalf;
        z = axis.z * sinHalf;
        w = cosHalf;
    }

    Quat& operator=(const Quat& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;
        w = other.w;

        return *this;
    }
};

inline Quat operator*(const Quat& a, const Quat& b) {
    Vec3 av(a.x, a.y, a.z);
    Vec3 bv(b.x, b.y, b.z);
    Vec3 v = Cross(av, bv) + (bv*a.w) + (av*b.w);
    float w = a.w*b.w - Dot(av, bv);
    return Quat(
        v.x,
        v.y,
        v.z,
        w
    );
}

// magnitude/length squared
inline float MagSqr(const Quat& q) {
    return q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w;
}

// magnitude/length
inline float Mag(const Quat& q) {
    return sqrtf(MagSqr(q));
}

inline Quat Normalize(const Quat& q) {
    float lenInv = 1.0f / Mag(q);
    return Quat(
        q.x * lenInv,
        q.y * lenInv,
        q.z * lenInv,
        q.w * lenInv
    );
}

// conjugate
inline Quat Conj(const Quat& q) {
    return Quat(
        -q.x,
        -q.y,
        -q.z,
        q.w
    );
}

// rotates v by q
// assumes q is normalized
inline Vec3 Transform(const Quat& q, const Vec3& v) {
    Vec3 u(q.x, q.y, q.z);
    return 
        (2.0f * Dot(u, v) * u +
        (q.w*q.w - Dot(u, u)) * v +
        2.0f * q.w * Cross(u, v));
}

struct Mat4
{
    // Column major ordering
    union
    {
        float v[4][4];
        struct { float r0c0, r1c0, r2c0, r3c0,
                       r0c1, r1c1, r2c1, r3c1,
                       r0c2, r1c2, r2c2, r3c2,
                       r0c3, r1c3, r2c3, r3c3; };
    };

    Mat4()
    {
    }

    Mat4(const Mat4& other) :
        r0c0(other.r0c0), r1c0(other.r1c0), r2c0(other.r2c0), r3c0(other.r3c0),
        r0c1(other.r0c1), r1c1(other.r1c1), r2c1(other.r2c1), r3c1(other.r3c1),
        r0c2(other.r0c2), r1c2(other.r1c2), r2c2(other.r2c2), r3c2(other.r3c2),
        r0c3(other.r0c3), r1c3(other.r1c3), r2c3(other.r2c3), r3c3(other.r3c3)
    {
    }

    Mat4(float r0c0, float r0c1, float r0c2, float r0c3,
         float r1c0, float r1c1, float r1c2, float r1c3,
         float r2c0, float r2c1, float r2c2, float r2c3,
         float r3c0, float r3c1, float r3c2, float r3c3) :
        r0c0(r0c0), r1c0(r1c0), r2c0(r2c0), r3c0(r3c0),
        r0c1(r0c1), r1c1(r1c1), r2c1(r2c1), r3c1(r3c1),
        r0c2(r0c2), r1c2(r1c2), r2c2(r2c2), r3c2(r3c2),
        r0c3(r0c3), r1c3(r1c3), r2c3(r2c3), r3c3(r3c3)
    {}

    inline float operator()(int row, int col) const {
        return v[col][row];
    }

    Mat4& operator=(const Mat4& other) {
        r0c0 = other.r0c0;
        r0c1 = other.r0c1;
        r0c2 = other.r0c2;
        r0c3 = other.r0c3;
        
        r1c0 = other.r1c0;
        r1c1 = other.r1c1;
        r1c2 = other.r1c2;
        r1c3 = other.r1c3;
        
        r2c0 = other.r2c0;
        r2c1 = other.r2c1;
        r2c2 = other.r2c2;
        r2c3 = other.r2c3;
        
        r3c0 = other.r3c0;
        r3c1 = other.r3c1;
        r3c2 = other.r3c2;
        r3c3 = other.r3c3;
        
        return *this;
    }
    
    Mat4& operator+=(const Mat4& other) {
        r0c0 += other.r0c0;
        r0c1 += other.r0c1;
        r0c2 += other.r0c2;
        r0c3 += other.r0c3;
        
        r1c0 += other.r1c0;
        r1c1 += other.r1c1;
        r1c2 += other.r1c2;
        r1c3 += other.r1c3;
        
        r2c0 += other.r2c0;
        r2c1 += other.r2c1;
        r2c2 += other.r2c2;
        r2c3 += other.r2c3;
        
        r3c0 += other.r3c0;
        r3c1 += other.r3c1;
        r3c2 += other.r3c2;
        r3c3 += other.r3c3;
        
        return *this;
    }

    // from scale, rotate, translate
    inline void FromSRT(
        Vec3& scale,
        Quat& rotate,
        Vec3& translate)
    {
        __asm__ volatile(
            // rotation quat to mtx in MAT1
            "ulv.q C200, %4\n" // mtx2 col0 = rotate quat
            "vmul.t C230, C200, C200\n" // col3 = q.xyz*q.xyz
            "vfim.s S233, 2.0\n" // mxt2 col3 row3 = 2.0
            "vscl.t C230, C230, S233\n" // col3 = 2*q.xyz*q.xyz
            "vmul.s S210, S200, S201\n" // col1 row0 = xy
            "vscl.t C211, C200, S203\n" // col1 row1-3 = w*xyz
            "vscl.p C220, C200, S202\n" // col2 row0-1 = z*xy

            "vmidt.q M100\n" // mtx1 = identity

            "vsub.s S100, S100, S231\n" // mtx1 row0 col0 -= twoy2
            "vsub.s S100, S100, S232\n" //      row0 col0 -= twoz2
            "vsub.s S111, S111, S230\n" //      row1 col1 -= twox2
            "vsub.s S111, S111, S232\n" //      row1 col1 -= twoz2
            "vsub.s S122, S122, S230\n" //      row2 col2 -= twox2
            "vsub.s S122, S122, S231\n" //      row2 col2 -= twoy2

            "vsub.s S110, S210, S213\n" // mtx1 col1 row0 = xy - wz
            "vadd.s S120, S220, S212\n" //      col2 row0 = xz + wy
            "vadd.s S101, S210, S213\n" //      col0 row1 = xy + wz
            "vsub.s S102, S220, S212\n" //      col0 row2 = xz - wy
            "vsub.s S121, S221, S211\n" //      col2 row1 = yz - wx
            "vadd.s S112, S221, S211\n" //      col1 row2 = yz + wx

            "vscl.p C120, C120, S233\n" // mtx1 col2 row0-1 *= 2.0
            "vscl.p R102, R102, S233\n" //      col0-1 row2 *= 2.0
            "vmul.s S110, S110, S233\n" //      col1 row0 *= 2.0
            "vmul.s S101, S101, S233\n" //      col0 row1 *= 2.0

            // scale mtx in MAT2
            "vmidt.q M200\n" // mtx2 = identity
            "mtv %1, S200\n" // mtx2 col0 row0 = scale x
            "mtv %2, S211\n" // mtx2 col1 row1 = scale y
            "mtv %3, S222\n" // mtx2 col2 row2 = scale z

            // multiply rot * scale into MAT0
            "vmmul.q M000, M100, M200\n"

            // set translation in MAT0
            "mtv %5, S030\n" // mtx0 col3 row0 = trans. x
            "mtv %6, S031\n" // mtx0 col3 row1 = trans. y
            "mtv %7, S032\n" // mtx0 col3 row2 = trans. z

            // store result from MAT0 into this->v
            "usv.q C000, 0 + %0\n"
            "usv.q C010, 16 + %0\n"
            "usv.q C020, 32 + %0\n"
            "usv.q C030, 48 + %0\n"

            // translate mtx in MAT1
            //"vmidt.q M100\n" // mtx1 = identity
            //"mtv %5, S130\n" // mtx1 col3 row0 = trans. x
            //"mtv %6, S131\n" // mtx1 col3 row1 = trans. y
            //"mtv %7, S132\n" // mtx1 col3 row2 = trans. z

            // multiply translate * (rot*scale) into MAT2
            //"vmmul.q M200, M100, M000\n"

            // store result from MAT2 into this->v
            //"usv.q C200, 0 + %0\n"
            //"usv.q C210, 16 + %0\n"
            //"usv.q C220, 32 + %0\n"
            //"usv.q C230, 48 + %0\n"

        :"=m"(this->v)
        :"r"(scale.x), "r"(scale.y), "r"(scale.z),
         "m"(rotate.v),
         "r"(translate.x), "r"(translate.y), "r"(translate.z));
    }

    // From scale, translate
    inline void FromST(Vec3& scale, Vec3& translate)
    {
        __asm__ volatile(
            "vmidt.q M000\n" // MAT0 = identity
            "mtv %1, S000\n" // mtx0 col0 row0 = scale x
            "mtv %2, S011\n" // mtx0 col1 row1 = scale y
            "mtv %3, S022\n" // mtx0 col2 row2 = scale z
            "mtv %4, S000\n" // mtx0 col3 row0 = trans. x
            "mtv %5, S011\n" // mtx0 col3 row1 = trans. y
            "mtv %6, S022\n" // mtx0 col3 row2 = trans. z

            "usv.q C000, 0 + %0\n" // save MAT0 into this->v
            "usv.q C010, 16 + %0\n"
            "usv.q C020, 32 + %0\n"
            "usv.q C030, 48 + %0\n"
        :"=m"(this->v)
        :"r"(scale.x), "r"(scale.y), "r"(scale.z),
         "r"(translate.x), "r"(translate.y), "r"(translate.z));
    }
    inline void FromST(float sx, float sy, float sz, float tx, float ty, float tz)
    {
        __asm__ volatile(
            "vmidt.q M000\n" // MAT0 = identity
            "mtv %1, S000\n" // mtx0 col0 row0 = scale x
            "mtv %2, S011\n" // mtx0 col1 row1 = scale y
            "mtv %3, S022\n" // mtx0 col2 row2 = scale z
            "mtv %4, S030\n" // mtx0 col3 row0 = trans. x
            "mtv %5, S031\n" // mtx0 col3 row1 = trans. y
            "mtv %6, S032\n" // mtx0 col3 row2 = trans. z

            "usv.q C000, 0 + %0\n" // save MAT0 into this->v
            "usv.q C010, 16 + %0\n"
            "usv.q C020, 32 + %0\n"
            "usv.q C030, 48 + %0\n"
        :"=m"(this->v)
        :"r"(sx), "r"(sy), "r"(sz),
         "r"(tx), "r"(ty), "r"(tz));
    }
 
    inline void Lerp(const Mat4& m1, const Mat4& m2, const float t)
    {
        const float rt = 1.0f - t;
        __asm__ volatile(
            "mtv %3, S200\n" // S200 = t
            "mtv %4, S201\n" // S201 = rt
            
            "ulv.q C000, 0 + %1\n" // mtx0 = m1
            "ulv.q C010, 16 + %1\n"
            "ulv.q C020, 32 + %1\n"
            "ulv.q C030, 48 + %1\n"

            "ulv.q C100, 0 + %2\n" // mtx1 = m2
            "ulv.q C110, 16 + %2\n"
            "ulv.q C120, 32 + %2\n"
            "ulv.q C130, 48 + %2\n"

            "vscl.q C000, C000, S201\n" // mtx0 *= rt
            "vscl.q C010, C010, S201\n"
            "vscl.q C020, C020, S201\n"
            "vscl.q C030, C030, S201\n"

            "vscl.q C100, C100, S200\n" // mtx1 *= t
            "vscl.q C110, C110, S200\n"
            "vscl.q C120, C120, S200\n"
            "vscl.q C130, C130, S200\n"

            "vadd.q C200, C000, C100\n" // mtx2 = mtx0 + mtx1
            "vadd.q C210, C010, C110\n"
            "vadd.q C220, C020, C120\n"
            "vadd.q C230, C030, C130\n"

            "usv.q C200, 0 + %0\n" // save result in this->v
            "usv.q C210, 16 + %0\n"
            "usv.q C220, 32 + %0\n"
            "usv.q C230, 48 + %0\n"
        :"=m"(this->v)
        :"m"(m1.v), "m"(m2.v), "r"(t), "r"(rt));
    }

};

inline Mat4 operator*(const Mat4& lhs, const Mat4& rhs) {
    /*Vec4 row0(
        lhs(0,0)*rhs(0,0) + lhs(0,1)*rhs(1,0) + lhs(0,2)*rhs(2,0) + lhs(0,3)*rhs(3,0),
        lhs(0,0)*rhs(0,1) + lhs(0,1)*rhs(1,1) + lhs(0,2)*rhs(2,1) + lhs(0,3)*rhs(3,1),
        lhs(0,0)*rhs(0,2) + lhs(0,1)*rhs(1,2) + lhs(0,2)*rhs(2,2) + lhs(0,3)*rhs(3,2),
        lhs(0,0)*rhs(0,3) + lhs(0,1)*rhs(1,3) + lhs(0,2)*rhs(2,3) + lhs(0,3)*rhs(3,3)
    );
    Vec4 row1(
        lhs(1,0)*rhs(0,0) + lhs(1,1)*rhs(1,0) + lhs(1,2)*rhs(2,0) + lhs(1,3)*rhs(3,0),
        lhs(1,0)*rhs(0,1) + lhs(1,1)*rhs(1,1) + lhs(1,2)*rhs(2,1) + lhs(1,3)*rhs(3,1),
        lhs(1,0)*rhs(0,2) + lhs(1,1)*rhs(1,2) + lhs(1,2)*rhs(2,2) + lhs(1,3)*rhs(3,2),
        lhs(1,0)*rhs(0,3) + lhs(1,1)*rhs(1,3) + lhs(1,2)*rhs(2,3) + lhs(1,3)*rhs(3,3)
    );
    Vec4 row2(
        lhs(2,0)*rhs(0,0) + lhs(2,1)*rhs(1,0) + lhs(2,2)*rhs(2,0) + lhs(2,3)*rhs(3,0),
        lhs(2,0)*rhs(0,1) + lhs(2,1)*rhs(1,1) + lhs(2,2)*rhs(2,1) + lhs(2,3)*rhs(3,1),
        lhs(2,0)*rhs(0,2) + lhs(2,1)*rhs(1,2) + lhs(2,2)*rhs(2,2) + lhs(2,3)*rhs(3,2),
        lhs(2,0)*rhs(0,3) + lhs(2,1)*rhs(1,3) + lhs(2,2)*rhs(2,3) + lhs(2,3)*rhs(3,3)
    );
    Vec4 row3(
        lhs(3,0)*rhs(0,0) + lhs(3,1)*rhs(1,0) + lhs(3,2)*rhs(2,0) + lhs(3,3)*rhs(3,0),
        lhs(3,0)*rhs(0,1) + lhs(3,1)*rhs(1,1) + lhs(3,2)*rhs(2,1) + lhs(3,3)*rhs(3,1),
        lhs(3,0)*rhs(0,2) + lhs(3,1)*rhs(1,2) + lhs(3,2)*rhs(2,2) + lhs(3,3)*rhs(3,2),
        lhs(3,0)*rhs(0,3) + lhs(3,1)*rhs(1,3) + lhs(3,2)*rhs(2,3) + lhs(3,3)*rhs(3,3)
    );
    return Mat4(
        row0.x, row0.y, row0.z, row0.w,
        row1.x, row1.y, row1.z, row1.w,
        row2.x, row2.y, row2.z, row2.w,
        row3.x, row3.y, row3.z, row3.w
    );*/
    Mat4 res;
    __asm__ volatile(
        "ulv.q C000, 0 + %1\n" // set MTX0 = lhs
        "ulv.q C010, 16 + %1\n"
        "ulv.q C020, 32 + %1\n"
        "ulv.q C030, 48 + %1\n"

        "ulv.q C100, 0 + %2\n" // set MTX1 = rhs
        "ulv.q C110, 16 + %2\n"
        "ulv.q C120, 32 + %2\n"
        "ulv.q C130, 48 + %2\n"

        "vmmul.q M200, M000, M100\n" // set MTX2 = MTX0 * MTX1

        "usv.q C200, 0 + %0\n" // set res = MTX2
        "usv.q C210, 16 + %0\n"
        "usv.q C220, 32 + %0\n"
        "usv.q C230, 48 + %0\n"
    :"=m"(res.v)
    :"m"(lhs.v), "m"(rhs.v));
    return res;
}
inline Mat4 operator*(const Mat4& lhs, const float rhs) {
    /*return Mat4(
        lhs(0,0)*rhs, lhs(0,1)*rhs, lhs(0,2)*rhs, lhs(0,3)*rhs,
        lhs(1,0)*rhs, lhs(1,1)*rhs, lhs(1,2)*rhs, lhs(1,3)*rhs,
        lhs(2,0)*rhs, lhs(2,1)*rhs, lhs(2,2)*rhs, lhs(2,3)*rhs,
        lhs(3,0)*rhs, lhs(3,1)*rhs, lhs(3,2)*rhs, lhs(3,3)*rhs
    );*/
    Mat4 res;
    __asm__ volatile(
        "ulv.q C000, 0 + %1\n" // set M000 = lhs
        "ulv.q C010, 16 + %1\n"
        "ulv.q C020, 32 + %1\n"
        "ulv.q C030, 48 + %1\n"

        "mtv %2, S100\n" // set mtx1 col0 row0 = rhs

        "vscl.q C000, C000, S100\n" // set mtx0 *= rhs
        "vscl.q C010, C010, S100\n"
        "vscl.q C020, C020, S100\n"
        "vscl.q C030, C030, S100\n"

        "usv.q C000, 0 + %0\n" // set res = M000
        "usv.q C010, 16 + %0\n"
        "usv.q C020, 32 + %0\n"
        "usv.q C030, 48 + %0\n"
    :"=m"(res.v)
    :"m"(lhs.v), "r"(rhs));
    return res;
}
inline Mat4 operator*(const float lhs, const Mat4& rhs) {
    return rhs * lhs;
}
inline Vec4 operator*(const Mat4& lhs, const Vec4& rhs) {
    return Vec4(
        lhs(0,0)*rhs.x + lhs(0,1)*rhs.y + lhs(0,2)*rhs.z + lhs(0,3)*rhs.w,
        lhs(1,0)*rhs.x + lhs(1,1)*rhs.y + lhs(1,2)*rhs.z + lhs(1,3)*rhs.w,
        lhs(2,0)*rhs.x + lhs(2,1)*rhs.y + lhs(2,2)*rhs.z + lhs(2,3)*rhs.w,
        lhs(3,0)*rhs.x + lhs(3,1)*rhs.y + lhs(3,2)*rhs.z + lhs(3,3)*rhs.w
    );
}
inline Mat4 operator+(const Mat4& lhs, const Mat4& rhs) {
    return Mat4(
        lhs(0,0)+rhs(0,0), lhs(0,1)+rhs(0,1), lhs(0,2)+rhs(0,2), lhs(0,3)+rhs(0,3),
        lhs(1,0)+rhs(1,0), lhs(1,1)+rhs(1,1), lhs(1,2)+rhs(1,2), lhs(1,3)+rhs(1,3),
        lhs(2,0)+rhs(2,0), lhs(2,1)+rhs(2,1), lhs(2,2)+rhs(2,2), lhs(2,3)+rhs(2,3),
        lhs(3,0)+rhs(3,0), lhs(3,1)+rhs(3,1), lhs(3,2)+rhs(3,2), lhs(3,3)+rhs(3,3)
    );
}

inline float Deg2Rad(const float deg) { return deg * M_PI / 180.0f; }
inline float Rad2Deg(const float rad) { return rad * 180.0f / M_PI; }
inline float Clamp(float val, const float min, float max) {
    return (val < min ? min : 
        (val > max ? max : val));
}

// multiplies v by m
inline Vec3 Transform(const Mat4& m, const Vec3& v, const float w = 1.0f) {
    return Vec3(
        m(0,0)*v.x + m(0,1)*v.y + m(0,2)*v.z + m(0,3)*w,
        m(1,0)*v.x + m(1,1)*v.y + m(1,2)*v.z + m(1,3)*w,
        m(2,0)*v.x + m(2,1)*v.y + m(2,2)*v.z + m(2,3)*w
    );
}

inline Mat4 Translate(const Vec3& trans) {
    return Mat4(
        1.0f, 0.0f, 0.0f, trans.x,
        0.0f, 1.0f, 0.0f, trans.y,
        0.0f, 0.0f, 1.0f, trans.z,
        0.0f, 0.0f, 0.0f, 1.0f
    );
}
inline Mat4 Translate(float x, float y, float z) {
    return Mat4(
        1.0f, 0.0f, 0.0f, x,
        0.0f, 1.0f, 0.0f, y,
        0.0f, 0.0f, 1.0f, z,
        0.0f, 0.0f, 0.0f, 1.0f
    );
}

inline Mat4 Rotate(const float angleRadians, const Vec3& axis) {
    const float c = cosf(angleRadians);
    const float s = sinf(angleRadians);
    
    const float axay = axis.x*axis.y;
    const float axaz = axis.x*axis.z;
    const float ayaz = axis.y*axis.z;
    const float ax2 = axis.x*axis.x;
    const float ay2 = axis.y*axis.y;
    const float az2 = axis.z*axis.z;

    return Mat4(
        c+(1-c)*ax2, (1-c)*axay-s*axis.z, (1-c)*axaz+s*axis.y, 0.0f,
        (1-c)*axay+s*axis.z, c+(1-c)*ay2, (1-c)*ayaz-s*axis.x, 0.0f,
        (1-c)*axaz-s*axis.y, (1-c)*ayaz+s*axis.x, c+(1-c)*az2, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    );
}

inline Mat4 Scale(const Vec3& s) {
    return Mat4(
        s.x, 0.0f, 0.0f, 0.0f,
        0.0f, s.y, 0.0f, 0.0f,
        0.0f, 0.0f, s.z, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    );
}
inline Mat4 Scale(float x, float y, float z) {
    return Mat4(
        x, 0.0f, 0.0f, 0.0f,
        0.0f, y, 0.0f, 0.0f,
        0.0f, 0.0f, z, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    );
}

inline Mat4 QuatToMat4(const Quat& q) {
    /*const float xy = q.x*q.y;
    const float wz = q.w*q.z;
    const float wy = q.w*q.y;
    const float wx = q.w*q.x;
    const float xz = q.x*q.z;
    const float yz = q.y*q.z;
    const float twox2 = 2.0f*q.x*q.x;
    const float twoy2 = 2.0f*q.y*q.y;
    const float twoz2 = 2.0f*q.z*q.z;
    
    return Mat4(
        1-twoy2-twoz2, 2*(xy-wz), 2*(xz+wy), 0.0f,
        2*(xy+wz), 1-twox2-twoz2, 2*(yz-wx), 0.0f,
        2*(xz-wy), 2*(yz+wx), 1-twox2-twoy2, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    );*/
    Mat4 res;
    __asm__ volatile(
        "ulv.q C200, %1\n" // mtx2 col0 = rotate quat
        "vmul.t C230, C200, C200\n" // col3 = q.xyz*q.xyz
        "vfim.s S233, 2.0\n" // mxt2 col3 row3 = 2.0
        "vscl.t C230, C230, S233\n" // col3 = 2*q.xyz*q.xyz
        "vmul.s S210, S200, S201\n" // col1 row0 = xy
        "vscl.t C211, C200, S203\n" // col1 row1-3 = w*xyz
        "vscl.p C220, C200, S202\n" // col2 row0-1 = z*xy

        "vmidt.q M100\n" // mtx1 = identity

        "vsub.s S100, S100, S231\n" // mtx1 row0 col0 -= twoy2
        "vsub.s S100, S100, S232\n" //      row0 col0 -= twoz2
        "vsub.s S111, S111, S230\n" //      row1 col1 -= twox2
        "vsub.s S111, S111, S232\n" //      row1 col1 -= twoz2
        "vsub.s S122, S122, S230\n" //      row2 col2 -= twox2
        "vsub.s S122, S122, S231\n" //      row2 col2 -= twoy2

        "vsub.s S110, S210, S213\n" // mtx1 col1 row0 = xy - wz
        "vadd.s S120, S220, S212\n" //      col2 row0 = xz + wy
        "vadd.s S101, S210, S213\n" //      col0 row1 = xy + wz
        "vsub.s S102, S220, S212\n" //      col0 row2 = xz - wy
        "vsub.s S121, S221, S211\n" //      col2 row1 = yz - wx
        "vadd.s S112, S221, S211\n" //      col1 row2 = yz + wx

        "vscl.p C120, C120, S233\n" // mtx1 col2 row0-1 *= 2.0
        "vscl.p R102, R102, S233\n" //      col0-1 row2 *= 2.0
        "vmul.s S110, S110, S233\n" //      col1 row0 *= 2.0
        "vmul.s S101, S101, S233\n" //      col0 row1 *= 2.0

        "usv.q C100, 0 + %0\n" // store result mtx col0
        "usv.q C110, 16 + %0\n" //                  col1
        "usv.q C120, 32 + %0\n" //                  col2
        "usv.q C130, 48 + %0\n" //                  col3
    :"=m"(res.v)
    :"m"(q.v));
    
    return res;
}

// Foundations of Game Engine Development Vol. 1 pg. 49
inline Mat4 Inverse(const Mat4& m) {
    Vec3 a(m(0,0), m(1,0), m(2,0));
    Vec3 b(m(0,1), m(1,1), m(2,1));
    Vec3 c(m(0,2), m(1,2), m(2,2));
    Vec3 d(m(0,3), m(1,3), m(2,3));
    
    const float x = m(3,0);
    const float y = m(3,1);
    const float z = m(3,2);
    const float w = m(3,3);
    
    Vec3 s = Cross(a, b);
    Vec3 t = Cross(c, d);
    Vec3 u = y*a - x*b;
    Vec3 v = w*c - z*d;
    
    float detInv = 1.0f / (Dot(s, v) + Dot(t, u));
    Vec3 row0 = (Cross(b, v) + y*t);
    Vec3 row1 = (Cross(v, a) - x*t);
    Vec3 row2 = (Cross(d, u) + w*s);
    Vec3 row3 = (Cross(u, c) - z*s);
    
    return Mat4(
        row0.x, row0.y, row0.z, -Dot(b, t),
        row1.x, row1.y, row1.z, Dot(a, t),
        row2.x, row2.y, row2.z, -Dot(d, s),
        row3.x, row3.y, row3.z, Dot(c, s)
    ) * detInv;
}

inline Mat4 Transpose(const Mat4& m) {
    return Mat4(
        m(0,0), m(1,0), m(2,0), m(3,0),
        m(0,1), m(1,1), m(2,1), m(3,1),
        m(0,2), m(1,2), m(2,2), m(3,2),
        m(0,3), m(1,3), m(2,3), m(3,3)
    );
}

// https://www.geertarien.com/blog/2017/07/30/breakdown-of-the-lookAt-function-in-OpenGL/
inline Mat4 LookAt(const Vec3& eye, const Vec3& at, const Vec3& up) {
    Vec3 zaxis = Normalize(at - eye);
    Vec3 xaxis = Normalize(Cross(zaxis, up));
    Vec3 yaxis = Cross(xaxis, zaxis);
    
    zaxis = zaxis * -1.0f;
    
    return Mat4(
        xaxis.x, xaxis.y, xaxis.z, -Dot(xaxis, eye),
        yaxis.x, yaxis.y, yaxis.z, -Dot(yaxis, eye),
        zaxis.x, zaxis.y, zaxis.z, -Dot(zaxis, eye),
        0.0f, 0.0f, 0.0f, 1.0f
    );
}

// https://github.com/ps3dev/PSL1GHT/blob/master/common/vectormath/ppu/cpp/mat_aos.h
inline Mat4 Perspective(const float fovRad, const float aspect, const float near, const float far) {
    Mat4 result;
    float f, rangeInv;
    Vec4 zero, col0, col1, col2, col3;
    Vec4 tmp;
    f = tanf((M_PI/2.0f) - fovRad * 0.5f);
    rangeInv = 1.0f / (near - far);
    zero = Vec4(0.0f, 0.0f, 0.0f, 0.0f);
    tmp = zero;
    tmp.v[0] = f / aspect;
    col0 = tmp;
    tmp = zero;
    tmp.v[1] = f;
    col1 = tmp;
    tmp = zero;
    tmp.v[2] = (near + far) * rangeInv;
    tmp.v[3] = -1.0f;
    col2 = tmp;
    tmp = zero;
    tmp.v[2] = near * far * rangeInv * 2.0f;
    col3 = tmp;
    //result.col0 = col0;
    //result.col1 = col1;
    //result.col2 = col2;
    //result.col3 = col3;
    //return result;
    return Mat4(
        col0.x, col1.x, col2.x, col3.x,
        col0.y, col1.y, col2.y, col3.y,
        col0.z, col1.z, col2.z, col3.z,
        col0.w, col1.w, col2.w, col3.w
    );
}

// adapted from raylib rcore.c
inline Vec2 WorldToScreenCoords(Vec3& pos, Mat4& viewMat, Mat4& projMat, int scrWidth, int scrHeight) {
    Mat4 viewProj = projMat * viewMat;
    // we want to calculate and include the w coord
    Vec4 worldPos = viewProj * Vec4(pos, 1.0f);
    float w = worldPos.w;
    if (fabsf(w) > 0.00001f) {
        w = 1.0f / w;
    }
    // normalized device coordinates (inverted y)
    Vec3 ndcPos(worldPos.x * w, -worldPos.y * w, worldPos.z * w);
    // 2d screen pos
    return Vec2(
        (ndcPos.x + 1.0f) / 2.0f * ((float)scrWidth),
        (ndcPos.y + 1.0f) / 2.0f * ((float)scrHeight)
    );
}

} // end namespace GameMath

#endif // GAMEMATH_PSP_H_INCLUDED

