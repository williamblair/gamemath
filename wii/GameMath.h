#ifndef GAMEMATH_WII_H_INCLUDED
#define GAMEMATH_WII_H_INCLUDED

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cstddef>
#include <gccore.h>

namespace GameMath
{

struct Vec2
{
    union
    {
        struct { f32 x, y; };
        struct { f32 u, v; };
    };

    Vec2() :
        x(0.0f),
        y(0.0f)
    {}

    Vec2(f32 x, f32 y) :
        x(x),
        y(y)
    {}

    Vec2(const Vec2& other) :
        x(other.x),
        y(other.y)
    {}

    Vec2& operator=(const Vec2& other)
    {
        x = other.x;
        y = other.y;
        return *this;
    }

    Vec2& operator+=(const Vec2& other)
    {
        x += other.x;
        y += other.y;
        return *this;
    }

    Vec2& operator-=(const Vec2& other)
    {
        x -= other.x;
        y -= other.y;
        return *this;
    }
};

inline Vec2 operator+(const Vec2& lhs, const Vec2& rhs) {
    return Vec2(
        lhs.x+rhs.x,
        lhs.y+rhs.y
    );
}
inline Vec2 operator-(const Vec2& lhs, const Vec2& rhs) {
    return Vec2(
        lhs.x-rhs.x,
        lhs.y-rhs.y
    );
}
inline Vec2 operator*(const Vec2& lhs, const float rhs) {
    return Vec2(
        lhs.x * rhs,
        lhs.y * rhs
    );
}
inline Vec2 operator*(const float& lhs, const Vec2& rhs) {
    return rhs * lhs;
}
inline Vec2 Normalize(const Vec2& v) {
    const float mag = sqrtf(v.x*v.x + v.y*v.y);
    return Vec2(
        v.x / mag,
        v.y / mag
    );
}

// Compatible with guVector
struct Vec3
{
    union
    {
        struct { f32 x,y,z; };
        f32 v[3];
    };

    Vec3() :
        x(0.0f),
        y(0.0f),
        z(0.0f)
    {}

    Vec3(f32 x, f32 y, f32 z) :
        x(x),
        y(y),
        z(z)
    {}

    Vec3(const Vec3& other) :
        x(other.x),
        y(other.y),
        z(other.z)
    {}

    Vec3& operator=(const Vec3& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;
        
        return *this;
    }

    Vec3& operator+=(const Vec3& other) {
        guVecAdd((guVector*)this, (guVector*)&other, (guVector*)this);

        return *this;
    }
    Vec3& operator-=(const Vec3& other) {
        guVecSub((guVector*)this, (guVector*)&other, (guVector*)this);

        return *this;
    }
};

struct Vec4
{
    union
    {
        struct { f32 x,y,z,w; };
        f32 v[4];
    };

    Vec4() :
        x(0.0f),
        y(0.0f),
        z(0.0f),
        w(1.0f)
    {}

    Vec4(f32 x, f32 y, f32 z, f32 w) :
        x(x),
        y(y),
        z(z),
        w(w)
    {}

    Vec4(const Vec4& other) :
        x(other.x),
        y(other.y),
        z(other.z),
        w(other.w)
    {}

    Vec4(const Vec3& vec, const float w) :
        x(vec.x),
        y(vec.y),
        z(vec.z),
        w(w)
    {}

    Vec4& operator=(const Vec4& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;
        w = other.w;
        
        return *this;
    }

    Vec4& operator+=(const Vec4& other) {
        x += other.x;
        y += other.y;
        z += other.z;
        w += other.w;

        return *this;
    }
    Vec4& operator-=(const Vec4& other) {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        w -= other.w;

        return *this;
    }
};

inline Vec3 operator+(const Vec3& lhs, const Vec3& rhs) {
    Vec3 result;
    guVecAdd((guVector*)&lhs, (guVector*)&rhs, (guVector*)&result);
    return result;
}
inline Vec3 operator-(const Vec3& lhs, const Vec3& rhs) {
    Vec3 result;
    guVecSub((guVector*)&lhs, (guVector*)&rhs, (guVector*)&result);
    return result;
}
inline Vec3 operator*(const Vec3& v, const f32 scale) {
    Vec3 result;
    guVecScale((guVector*)&v, (guVector*)&result, scale);
    return result;
}
inline Vec3 operator*(const f32 scale, const Vec3& v) {
    Vec3 result;
    guVecScale((guVector*)&v, (guVector*)&result, scale);
    return result;
}
inline f32 Dot(const Vec3& lhs, const Vec3& rhs) {
    return guVecDotProduct((guVector*)&lhs, (guVector*)&rhs);
}
inline Vec3 Cross(const Vec3& lhs, const Vec3& rhs) {
    Vec3 result;
    guVecCross((guVector*)&lhs, (guVector*)&rhs, (guVector*)&result);
    return result;
}
inline Vec3 Normalize(const Vec3& v) {
    Vec3 result(v);
    guVecNormalize((guVector*)&v);
    return result;
}


// compatible with guQuaternion
struct Quat
{
    union
    {
        struct { f32 x,y,z,w; };
        f32 data[4];
    };

    Quat() :
        x(0.0f),
        y(0.0f),
        z(0.0f),
        w(1.0f)
    {}

    Quat(f32 x, f32 y, f32 z, f32 w) :
        x(x),
        y(y),
        z(z),
        w(w)
    {}

    Quat(const f32 angleRadians, const Vec3& axis)
    {
        const f32 sinHalf = sinf(angleRadians * 0.5f);
        const f32 cosHalf = cosf(angleRadians * 0.5f);
        x = axis.x * sinHalf;
        y = axis.y * sinHalf;
        z = axis.z * sinHalf;
        w = cosHalf;
    }

    Quat& operator=(const Quat& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;
        w = other.w;

        return *this;
    }
};

inline Quat operator*(const Quat& a, const Quat& b) {
    //Vec3 av(a.x, a.y, a.z);
    //Vec3 bv(b.x, b.y, b.z);
    //Vec3 v = Cross(av, bv) + (bv*a.w) + (av*b.w);
    //f32 w = a.w*b.w - Dot(av, bv);
    //return Quat(
    //    v.x,
    //    v.y,
    //    v.z,
    //    w
    //);
    Quat result;
    guQuatMultiply((guQuaternion*)&a, (guQuaternion*)&b, (guQuaternion*)&result);
    return result;
}

// magnitude/length squared
inline f32 MagSqr(const Quat& q) {
    return q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w;
}

// magnitude/length
inline f32 Mag(const Quat& q) {
    return sqrtf(MagSqr(q));
}

inline Quat Normalize(const Quat& q) {
    Quat result;
    guQuatNormalize((guQuaternion*)&q, (guQuaternion*)&result);
    return result;
}

// conjugate
inline Quat Conj(const Quat& q) {
    return Quat(
        -q.x,
        -q.y,
        -q.z,
        q.w
    );
}

// rotates v by q
// assumes q is normalized
inline Vec3 Transform(const Quat& q, const Vec3& v) {
    Vec3 u(q.x, q.y, q.z);
    return 
        (2.0f * Dot(u, v) * u +
        (q.w*q.w - Dot(u, u)) * v +
        2.0f * q.w * Cross(u, v));
}

struct Mat4
{
    // Row major ordering
    union
    {
        f32 v[4][4];
        struct { f32 r0c0, r0c1, r0c2, r0c3,
                     r1c0, r1c1, r1c2, r1c3,
                     r2c0, r2c1, r2c2, r2c3,
                     r3c0, r3c1, r3c2, r3c3; };
    };

    Mat4() :
        r0c0(1.0f), r0c1(0.0f), r0c2(0.0f), r0c3(0.0f),
        r1c0(0.0f), r1c1(1.0f), r1c2(0.0f), r1c3(0.0f),
        r2c0(0.0f), r2c1(0.0f), r2c2(1.0f), r2c3(0.0f),
        r3c0(0.0f), r3c1(0.0f), r3c2(0.0f), r3c3(1.0f)
    {
    }

    Mat4(const Mat4& other) :
        r0c0(other.r0c0), r0c1(other.r0c1), r0c2(other.r0c2), r0c3(other.r0c3),
        r1c0(other.r1c0), r1c1(other.r1c1), r1c2(other.r1c2), r1c3(other.r1c3),
        r2c0(other.r2c0), r2c1(other.r2c1), r2c2(other.r2c2), r2c3(other.r2c3),
        r3c0(other.r3c0), r3c1(other.r3c1), r3c2(other.r3c2), r3c3(other.r3c3)
    {
    }

    Mat4(f32 r0c0, f32 r0c1, f32 r0c2, f32 r0c3,
         f32 r1c0, f32 r1c1, f32 r1c2, f32 r1c3,
         f32 r2c0, f32 r2c1, f32 r2c2, f32 r2c3,
         f32 r3c0, f32 r3c1, f32 r3c2, f32 r3c3) :
        r0c0(r0c0), r0c1(r0c1), r0c2(r0c2), r0c3(r0c3),
        r1c0(r1c0), r1c1(r1c1), r1c2(r1c2), r1c3(r1c3),
        r2c0(r2c0), r2c1(r2c1), r2c2(r2c2), r2c3(r2c3),
        r3c0(r3c0), r3c1(r3c1), r3c2(r3c2), r3c3(r3c3)
    {}

    inline f32 operator()(int row, int col) const {
        return v[row][col];
    }

    Mat4& operator=(const Mat4& other) {
        guMtx44Copy((f32(*)[4])other.v, (f32(*)[4])this->v);
        return *this;
    }
    
    Mat4& operator+=(const Mat4& other) {
        r0c0 += other.r0c0;
        r0c1 += other.r0c1;
        r0c2 += other.r0c2;
        r0c3 += other.r0c3;
        
        r1c0 += other.r1c0;
        r1c1 += other.r1c1;
        r1c2 += other.r1c2;
        r1c3 += other.r1c3;
        
        r2c0 += other.r2c0;
        r2c1 += other.r2c1;
        r2c2 += other.r2c2;
        r2c3 += other.r2c3;
        
        r3c0 += other.r3c0;
        r3c1 += other.r3c1;
        r3c2 += other.r3c2;
        r3c3 += other.r3c3;
        
        return *this;
    }
};

inline Mat4 operator*(const Mat4& lhs, const Mat4& rhs) {
    Mat4 result;
    guMtxConcat((f32(*)[4])lhs.v, (f32(*)[4])rhs.v, (f32(*)[4])result.v);
    return result;
}
inline Mat4 operator*(const Mat4& lhs, const f32 rhs) {
    return Mat4(
        lhs(0,0)*rhs, lhs(0,1)*rhs, lhs(0,2)*rhs, lhs(0,3)*rhs,
        lhs(1,0)*rhs, lhs(1,1)*rhs, lhs(1,2)*rhs, lhs(1,3)*rhs,
        lhs(2,0)*rhs, lhs(2,1)*rhs, lhs(2,2)*rhs, lhs(2,3)*rhs,
        lhs(3,0)*rhs, lhs(3,1)*rhs, lhs(3,2)*rhs, lhs(3,3)*rhs
    );
}
inline Mat4 operator*(const f32 lhs, const Mat4& rhs) {
    return rhs * lhs;
}
inline Vec4 operator*(const Mat4& lhs, const Vec4& rhs) {
    return Vec4(
        lhs(0,0)*rhs.x + lhs(0,1)*rhs.y + lhs(0,2)*rhs.z + lhs(0,3)*rhs.w,
        lhs(1,0)*rhs.x + lhs(1,1)*rhs.y + lhs(1,2)*rhs.z + lhs(1,3)*rhs.w,
        lhs(2,0)*rhs.x + lhs(2,1)*rhs.y + lhs(2,2)*rhs.z + lhs(2,3)*rhs.w,
        lhs(3,0)*rhs.x + lhs(3,1)*rhs.y + lhs(3,2)*rhs.z + lhs(3,3)*rhs.w
    );
}
inline Mat4 operator+(const Mat4& lhs, const Mat4& rhs) {
    return Mat4(
        lhs(0,0)+rhs(0,0), lhs(0,1)+rhs(0,1), lhs(0,2)+rhs(0,2), lhs(0,3)+rhs(0,3),
        lhs(1,0)+rhs(1,0), lhs(1,1)+rhs(1,1), lhs(1,2)+rhs(1,2), lhs(1,3)+rhs(1,3),
        lhs(2,0)+rhs(2,0), lhs(2,1)+rhs(2,1), lhs(2,2)+rhs(2,2), lhs(2,3)+rhs(2,3),
        lhs(3,0)+rhs(3,0), lhs(3,1)+rhs(3,1), lhs(3,2)+rhs(3,2), lhs(3,3)+rhs(3,3)
    );
}

inline f32 Deg2Rad(const f32 deg) { return deg * M_PI / 180.0f; }
inline f32 Rad2Deg(const f32 rad) { return rad * 180.0f / M_PI; }
inline f32 Clamp(f32 val, const f32 min, f32 max) {
    return (val < min ? min : 
        (val > max ? max : val));
}

// multiplies v by m
inline Vec3 Transform(const Mat4& m, const Vec3& v, const f32 w = 1.0f) {
    Vec3 result;
    guVecMultiply((f32(*)[4])m.v, (guVector*)&v, (guVector*)&result);
    return result;
}

inline Mat4 Translate(const Vec3& trans) {
    Mat4 result;
    guMtxTrans((f32(*)[4])result.v, trans.x,trans.y,trans.z);
    return result;
}
inline Mat4 Translate(f32 x, f32 y, f32 z) {
    Mat4 result;
    guMtxTrans((f32(*)[4])result.v, x,y,z);
    return result;
}

inline Mat4 Rotate(const f32 angleRadians, const Vec3& axis) {
    Mat4 result;
    guMtxRotAxisRad((f32(*)[4])result.v, (guVector*)&axis, angleRadians);
    return result;
}

inline Mat4 Scale(const Vec3& s) {
    Mat4 result;
    guMtxScale((f32(*)[4])result.v, s.x,s.y,s.z);
    return result;
}
inline Mat4 Scale(f32 x, f32 y, f32 z) {
    Mat4 result;
    guMtxScale((f32(*)[4])result.v, x,y,z);
    return result;
}

inline Mat4 Inverse(const Mat4& m) {
    Mat4 result;
    guMtx44Inverse((f32(*)[4])m.v, (f32(*)[4])result.v);
    return result;
}

inline Mat4 Transpose(const Mat4& m) {
    return Mat4(
        m(0,0), m(1,0), m(2,0), m(3,0),
        m(0,1), m(1,1), m(2,1), m(3,1),
        m(0,2), m(1,2), m(2,2), m(3,2),
        m(0,3), m(1,3), m(2,3), m(3,3)
    );
}

inline Mat4 QuatToMat4(const Quat& q) {
    Mat4 result;
    c_guMtxQuat((f32(*)[4])result.v, (guQuaternion*)&q);
    // not sure why this needs to be transposed...
    return Transpose(result);
}

inline Mat4 LookAt(const Vec3& eye, const Vec3& at, const Vec3& up) {
    Mat4 result;
    guLookAt((f32(*)[4])result.v, (guVector*)&eye, (guVector*)&up, (guVector*)&at);
    return result;
}

// adapted from raylib rcore.c
inline Vec2 WorldToScreenCoords(Vec3& pos, Mat4& viewMat, Mat4& projMat, int scrWidth, int scrHeight) {
    Mat4 viewProj = projMat * viewMat;
    // the 4th row is not calculated by projMat*viewMat (only does 3x4)
    viewProj.r3c0 = projMat(3,0)*viewMat(0,0) + projMat(3,1)*viewMat(1,0) + projMat(3,2)*viewMat(2,0) + projMat(3,3)*viewMat(3,0);
    viewProj.r3c1 = projMat(3,0)*viewMat(0,1) + projMat(3,1)*viewMat(1,1) + projMat(3,2)*viewMat(2,1) + projMat(3,3)*viewMat(3,1);
    viewProj.r3c2 = projMat(3,0)*viewMat(0,2) + projMat(3,1)*viewMat(1,2) + projMat(3,2)*viewMat(2,2) + projMat(3,3)*viewMat(3,2);
    viewProj.r3c3 = projMat(3,0)*viewMat(0,3) + projMat(3,1)*viewMat(1,3) + projMat(3,2)*viewMat(2,3) + projMat(3,3)*viewMat(3,3);
    // we want to calculate and include the w coord
    Vec4 worldPos = viewProj * Vec4(pos, 1.0f);
    float w = worldPos.w;
    if (fabsf(w) > 0.00001f) {
        w = 1.0f / w;
    }
    // normalized device coordinates (inverted y)
    Vec3 ndcPos(worldPos.x * w, -worldPos.y * w, worldPos.z * w);
    // 2d screen pos
    return Vec2(
        (ndcPos.x + 1.0f) / 2.0f * ((float)scrWidth),
        (ndcPos.y + 1.0f) / 2.0f * ((float)scrHeight)
    );
}

} // end namespace GameMath

#endif // GAMEMATH_CPP_H_INCLUDED

