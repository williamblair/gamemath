#ifndef GAMEMATH_PS3_H_INCLUDED
#define GAMEMATH_PS3_H_INCLUDED

#include <cmath>
#include <cstring>
#include <altivec.h>
#include <vec_types.h>
#include <simdmath/simdmath.h>

// from vec_aos.h
#ifndef _VECTORMATH_PERM_X
#define _VECTORMATH_PERM_X 0x00010203
#define _VECTORMATH_PERM_Y 0x04050607
#define _VECTORMATH_PERM_Z 0x08090a0b
#define _VECTORMATH_PERM_W 0x0c0d0e0f
#define _VECTORMATH_PERM_A 0x10111213
#define _VECTORMATH_PERM_B 0x14151617
#define _VECTORMATH_PERM_C 0x18191a1b
#define _VECTORMATH_PERM_D 0x1c1d1e1f
#define _VECTORMATH_PERM_XYZA (vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_X, _VECTORMATH_PERM_Y, _VECTORMATH_PERM_Z, _VECTORMATH_PERM_A }
#define _VECTORMATH_PERM_ZXYW (vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Z, _VECTORMATH_PERM_X, _VECTORMATH_PERM_Y, _VECTORMATH_PERM_W }
#define _VECTORMATH_PERM_YZXW (vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Y, _VECTORMATH_PERM_Z, _VECTORMATH_PERM_X, _VECTORMATH_PERM_W }
#define _VECTORMATH_PERM_YZAB (vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Y, _VECTORMATH_PERM_Z, _VECTORMATH_PERM_A, _VECTORMATH_PERM_B }
#define _VECTORMATH_PERM_ZABC (vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Z, _VECTORMATH_PERM_A, _VECTORMATH_PERM_B, _VECTORMATH_PERM_C }
#define _VECTORMATH_PERM_XYAW (vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_X, _VECTORMATH_PERM_Y, _VECTORMATH_PERM_A, _VECTORMATH_PERM_W }
#define _VECTORMATH_PERM_XAZW (vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_X, _VECTORMATH_PERM_A, _VECTORMATH_PERM_Z, _VECTORMATH_PERM_W }
#define _VECTORMATH_MASK_0xF000 (vec_uint4){ 0xffffffff, 0, 0, 0 }
#define _VECTORMATH_MASK_0x0F00 (vec_uint4){ 0, 0xffffffff, 0, 0 }
#define _VECTORMATH_MASK_0x00F0 (vec_uint4){ 0, 0, 0xffffffff, 0 }
#define _VECTORMATH_MASK_0x000F (vec_uint4){ 0, 0, 0, 0xffffffff }
#define _VECTORMATH_UNIT_1000 (vec_float4){ 1.0f, 0.0f, 0.0f, 0.0f }
#define _VECTORMATH_UNIT_0100 (vec_float4){ 0.0f, 1.0f, 0.0f, 0.0f }
#define _VECTORMATH_UNIT_0010 (vec_float4){ 0.0f, 0.0f, 1.0f, 0.0f }
#define _VECTORMATH_UNIT_0001 (vec_float4){ 0.0f, 0.0f, 0.0f, 1.0f }

// mat_aos.h
#define _VECTORMATH_PERM_ZBWX ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Z, _VECTORMATH_PERM_B, _VECTORMATH_PERM_W, _VECTORMATH_PERM_X })
#define _VECTORMATH_PERM_XCYX ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_X, _VECTORMATH_PERM_C, _VECTORMATH_PERM_Y, _VECTORMATH_PERM_X })
#define _VECTORMATH_PERM_XYAB ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_X, _VECTORMATH_PERM_Y, _VECTORMATH_PERM_A, _VECTORMATH_PERM_B })
#define _VECTORMATH_PERM_ZWCD ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Z, _VECTORMATH_PERM_W, _VECTORMATH_PERM_C, _VECTORMATH_PERM_D })
#define _VECTORMATH_PERM_XZBX ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_X, _VECTORMATH_PERM_Z, _VECTORMATH_PERM_B, _VECTORMATH_PERM_X })     
#define _VECTORMATH_PERM_CXXX ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_C, _VECTORMATH_PERM_X, _VECTORMATH_PERM_X, _VECTORMATH_PERM_X })
#define _VECTORMATH_PERM_YAXX ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Y, _VECTORMATH_PERM_A, _VECTORMATH_PERM_X, _VECTORMATH_PERM_X })
#define _VECTORMATH_PERM_XAZC ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_X, _VECTORMATH_PERM_A, _VECTORMATH_PERM_Z, _VECTORMATH_PERM_C })
#define _VECTORMATH_PERM_YXWZ ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Y, _VECTORMATH_PERM_X, _VECTORMATH_PERM_W, _VECTORMATH_PERM_Z })
#define _VECTORMATH_PERM_YBWD ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Y, _VECTORMATH_PERM_B, _VECTORMATH_PERM_W, _VECTORMATH_PERM_D })
#define _VECTORMATH_PERM_XYCX ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_X, _VECTORMATH_PERM_Y, _VECTORMATH_PERM_C, _VECTORMATH_PERM_X })
#define _VECTORMATH_PERM_YCXY ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Y, _VECTORMATH_PERM_C, _VECTORMATH_PERM_X, _VECTORMATH_PERM_Y })
#define _VECTORMATH_PERM_CXYC ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_C, _VECTORMATH_PERM_X, _VECTORMATH_PERM_Y, _VECTORMATH_PERM_C })
#define _VECTORMATH_PERM_ZAYX ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Z, _VECTORMATH_PERM_A, _VECTORMATH_PERM_Y, _VECTORMATH_PERM_X })
#define _VECTORMATH_PERM_BZXX ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_B, _VECTORMATH_PERM_Z, _VECTORMATH_PERM_X, _VECTORMATH_PERM_X })
#define _VECTORMATH_PERM_XZYA ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_X, _VECTORMATH_PERM_Z, _VECTORMATH_PERM_Y, _VECTORMATH_PERM_A })
#define _VECTORMATH_PERM_ZXXB ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Z, _VECTORMATH_PERM_X, _VECTORMATH_PERM_X, _VECTORMATH_PERM_B })
#define _VECTORMATH_PERM_YXXC ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_Y, _VECTORMATH_PERM_X, _VECTORMATH_PERM_X, _VECTORMATH_PERM_C })
#define _VECTORMATH_PERM_BBYX ((vec_uchar16)(vec_uint4){ _VECTORMATH_PERM_B, _VECTORMATH_PERM_B, _VECTORMATH_PERM_Y, _VECTORMATH_PERM_X })
#define _VECTORMATH_PI_OVER_2 1.570796327f
#endif

namespace GameMath
{

struct Vec2
{
    union
    {
        struct { float x, y; };
        struct { float u, v; };
    };

    Vec2() :
        x(0.0f),
        y(0.0f)
    {}

    Vec2(float x, float y) :
        x(x),
        y(y)
    {}

    Vec2(const Vec2& other) :
        x(other.x),
        y(other.y)
    {}

    Vec2& operator=(const Vec2& other)
    {
        x = other.x;
        y = other.y;
        return *this;
    }

    Vec2& operator+=(const Vec2& other)
    {
        x += other.x;
        y += other.y;
        return *this;
    }

    Vec2& operator-=(const Vec2& other)
    {
        x -= other.x;
        y -= other.y;
        return *this;
    }
};

inline Vec2 operator+(const Vec2& lhs, const Vec2& rhs) {
    return Vec2(
        lhs.x+rhs.x,
        lhs.y+rhs.y
    );
}
inline Vec2 operator-(const Vec2& lhs, const Vec2& rhs) {
    return Vec2(
        lhs.x-rhs.x,
        lhs.y-rhs.y
    );
}
inline Vec2 operator*(const Vec2& lhs, const float rhs) {
    return Vec2(
        lhs.x * rhs,
        lhs.y * rhs
    );
}
inline Vec2 operator*(const float& lhs, const Vec2& rhs) {
    return rhs * lhs;
}
inline Vec2 Normalize(const Vec2& v) {
    const float mag = sqrtf(v.x*v.x + v.y*v.y);
    return Vec2(
        v.x / mag,
        v.y / mag
    );
}
inline bool operator==(const Vec2& lhs, const Vec2& rhs) {
    return (fabsf(lhs.x - rhs.x) < 0.0001f) &&
        (fabsf(lhs.y - rhs.y) < 0.0001f);
}

struct Vec4;
struct Vec3
{
    union
    {
        struct { float x,y,z; };
        float v[3];
    };

    Vec3() :
        x(0.0f),
        y(0.0f),
        z(0.0f)
    {}

    Vec3(float x, float y, float z) :
        x(x),
        y(y),
        z(z)
    {}

    Vec3(const Vec3& other) :
        x(other.x),
        y(other.y),
        z(other.z)
    {}
    
    //inline Vec3(const Vec4 other);
    inline Vec3(const Vec4& other);

    Vec3& operator=(const Vec3& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;
        
        return *this;
    }

    Vec3& operator+=(const Vec3& other) {
        x += other.x;
        y += other.y;
        z += other.z;

        return *this;
    }
    Vec3& operator-=(const Vec3& other) {
        x -= other.x;
        y -= other.y;
        z -= other.z;

        return *this;
    }
};

struct Vec4
{
    union
    {
        struct { float x,y,z,w; };
        float v[4];
        vec_float4 vec;
    };

    Vec4() :
        x(0.0f),
        y(0.0f),
        z(0.0f),
        w(1.0f)
    {}

    Vec4(float x, float y, float z, float w) :
        x(x),
        y(y),
        z(z),
        w(w)
    {}

    Vec4(const Vec4& other) :
        vec(other.vec)
    {}
    
    Vec4(const vec_float4& vec) :
        vec(vec)
    {}
    /*Vec4(const vec_float4 vec) :
        vec(vec)
    {}*/

    Vec4(const Vec3& vec, const float w) :
        x(vec.x),
        y(vec.y),
        z(vec.z),
        w(w)
    {}

    Vec4& operator=(const Vec4& other)
    {
        this->vec = other.vec;   
        return *this;
    }

    Vec4& operator+=(const Vec4& other) {
        this->vec = vec_add(this->vec, other.vec);
        return *this;
    }
    Vec4& operator-=(const Vec4& other) {
        //x -= other.x;
        //y -= other.y;
        //z -= other.z;
        //w -= other.w;
        this->vec = vec_sub(this->vec, other.vec);

        return *this;
    }
};

/*inline Vec3::Vec3(const Vec4 other) :
    x(other.x),
    y(other.y),
    z(other.z)
{}*/
inline Vec3::Vec3(const Vec4& other) :
    x(other.x),
    y(other.y),
    z(other.z)
{}

inline Vec3 operator+(const Vec3& lhs, const Vec3& rhs) {
    return Vec3(
        lhs.x + rhs.x,
        lhs.y + rhs.y,
        lhs.z + rhs.z
    );
}
inline Vec3 operator-(const Vec3& lhs, const Vec3& rhs) {
    return Vec3(
        lhs.x - rhs.x,
        lhs.y - rhs.y,
        lhs.z - rhs.z
    );
}
inline Vec3 operator*(const Vec3& lhs, const Vec3& rhs) {
    return Vec3(
        lhs.x * rhs.x,
        lhs.y * rhs.y,
        lhs.z * rhs.z
    );
}
inline Vec3 operator*(const Vec3& v, const float scale) {
    return Vec3(
        v.x * scale,
        v.y * scale,
        v.z * scale
    );
}
inline Vec3 operator*(const float scale, const Vec3& v) {
    return Vec3(
        v.x * scale,
        v.y * scale,
        v.z * scale
    );
}
inline bool operator==(const Vec3& lhs, const Vec3& rhs) {
    return (fabsf(lhs.x - rhs.x) < 0.0001f) &&
        (fabsf(lhs.y - rhs.y) < 0.0001f) &&
        (fabsf(lhs.z - rhs.z) < 0.0001f);
}
inline bool operator!=(const Vec3& lhs, const Vec3& rhs) {
    return !(lhs == rhs);
}
inline float MagSqr(const Vec3& v) {
    return v.x*v.x + v.y*v.y + v.z*v.z;
}
inline float Mag(const Vec3& v) {
    return sqrtf(MagSqr(v));
}
inline float Dot(const Vec3& lhs, const Vec3& rhs) {
    return (lhs.x*rhs.x + lhs.y*rhs.y + lhs.z*rhs.z);
}
inline Vec3 Cross(const Vec3& lhs, const Vec3& rhs) {
    return Vec3(
        lhs.y*rhs.z - lhs.z*rhs.y,
        lhs.z*rhs.x - lhs.x*rhs.z,
        lhs.x*rhs.y - lhs.y*rhs.x
    );
}
inline Vec3 Normalize(const Vec3& v) {
    const float magInv = 1.0f / sqrtf(v.x*v.x + v.y*v.y + v.z*v.z);
    return Vec3(
        v.x * magInv,
        v.y * magInv,
        v.z * magInv
    );
}
inline Vec3 Lerp(const Vec3& lhs, const Vec3& rhs, const float t) {
    return ((1.0f - t)*lhs) + (t*rhs);
}

inline Vec4 operator+(const Vec4& lhs, const Vec4& rhs) {
    /*Vec4 res;
    res.vec = vec_add(lhs.vec, rhs.vec);
    return res;*/
    return Vec4(vec_add(lhs.vec,rhs.vec));
}

inline Vec4 operator*(const Vec4& lhs, const float rhs) {
    /*Vec4 res;
    res.vec = vec_madd(lhs.vec, vec_splats(rhs), (vec_float4){0.0f,0.0f,0.0f,0.0f});
    return res;*/
    return Vec4(
        vec_madd(lhs.vec, vec_splats(rhs), (vec_float4){0.0f,0.0f,0.0f,0.0f})
    );
}
inline Vec4 operator*(const float lhs, const Vec4& rhs) {
    return rhs * lhs;
}

inline bool operator==(const Vec4& lhs, const Vec4& rhs) {
    return (fabsf(lhs.x - rhs.x) < 0.0001f) &&
        (fabsf(lhs.y - rhs.y) < 0.0001f) &&
        (fabsf(lhs.z - rhs.z) < 0.0001f) &&
        (fabsf(lhs.w - rhs.w) < 0.0001f);
}

struct Quat
{
    union
    {
        struct { float x,y,z,w; };
        float data[4];
        vec_float4 vec;
    };

    Quat() :
        x(0.0f),
        y(0.0f),
        z(0.0f),
        w(1.0f)
    {}

    Quat(float x, float y, float z, float w) :
        x(x),
        y(y),
        z(z),
        w(w)
    {}

    Quat(const float angleRadians, const Vec3& axis)
    {
        const float sinHalf = sinf(angleRadians * 0.5f);
        const float cosHalf = cosf(angleRadians * 0.5f);
        x = axis.x * sinHalf;
        y = axis.y * sinHalf;
        z = axis.z * sinHalf;
        w = cosHalf;
    }
    
    Quat(vec_float4 vec) :
        vec(vec)
    {}

    Quat& operator=(const Quat& other)
    {
        //x = other.x;
        //y = other.y;
        //z = other.z;
        //w = other.w;
        vec = other.vec;

        return *this;
    }
};

inline Quat operator*(const Quat& a, const Quat& b) {
    /*Vec3 av(a.x, a.y, a.z);
    Vec3 bv(b.x, b.y, b.z);
    Vec3 v = Cross(av, bv) + (bv*a.w) + (av*b.w);
    float w = a.w*b.w - Dot(av, bv);
    return Quat(
        v.x,
        v.y,
        v.z,
        w
    );*/
    return Quat(
        b.x*a.w + b.y*a.z - b.z*a.y + b.w*a.x,
        -b.x*a.z + b.y*a.w + b.z*a.x + b.w*a.y,
        b.x*a.y - b.y*a.x + b.z*a.w + b.w*a.z,
        -b.x*a.x - b.y*a.y - b.z*a.z + b.w*a.w
    );
}
inline Quat operator*(const Quat& a, const float t) {
    /*return Quat(
        a.x * t,
        a.y * t,
        a.z * t,
        a.w * t
    );*/
    return Quat(a.vec * t);
}
inline Quat operator+(const Quat& a, const Quat& b) {
    /*return Quat(
        a.x + b.x,
        a.y + b.y,
        a.z + b.z,
        a.w + b.w
    );*/
    return Quat(a.vec + b.vec);
}
inline Quat operator-(const Quat& a, const Quat& b) {
    /*return Quat(
        a.x - b.x,
        a.y - b.y,
        a.z - b.z,
        a.w - b.w
    );*/
    return Quat(vec_sub(a.vec, b.vec));
}

inline Quat operator-(const Quat& a) {
    return Quat(-a.x, -a.y, -a.z, -a.w);
}

inline bool operator==(const Quat& lhs, const Quat& rhs) {
    return (fabsf(lhs.x - rhs.x) < 0.000001f) &&
        (fabsf(lhs.y - rhs.y) < 0.000001f) &&
        (fabsf(lhs.z - rhs.z) < 0.000001f) &&
        (fabsf(lhs.w - rhs.w) < 0.000001f);
}
inline bool operator!=(const Quat& lhs, const Quat& rhs) {
    return !(lhs == rhs);
}

// magnitude/length squared
inline float MagSqr(const Quat& q) {
    Vec4 v(vec_madd(q.vec,q.vec,(vec_float4){0.0f,0.0f,0.0f,0.0f}));
    //return q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w;
    return v.x + v.y + v.z + v.w;
}

// magnitude/length
inline float Mag(const Quat& q) {
    return sqrtf(MagSqr(q));
}

inline float Dot(const Quat& lhs, const Quat& rhs) {
    //return lhs.x*rhs.x + lhs.y*rhs.y + lhs.z*rhs.z + lhs.w*rhs.w;
    Vec4 v(vec_madd(lhs.vec,rhs.vec,(vec_float4){0.0f,0.0f,0.0f,0.0f}));
    return v.x + v.y + v.z + v.w;
}

inline Quat Normalize(const Quat& q) {
    //float lenInv = 1.0f / Mag(q);
    /*return Quat(
        q.x * lenInv,
        q.y * lenInv,
        q.z * lenInv,
        q.w * lenInv
    );*/
    return Quat(
        vec_madd(
            q.vec,
            vec_splats(/*lenInv*/1.0f/Mag(q)),
            (vec_float4){0.0f,0.0f,0.0f,0.0f}
        )
    );
}

inline Quat Lerp(const Quat& lhs, const Quat& rhs, const float t) {
    return lhs + (rhs - lhs)*t;
}
inline Quat NLerp(const Quat& lhs, const Quat& rhs, const float t) {
    return Normalize(lhs + (rhs - lhs)*t);
}

// conjugate
inline Quat Conj(const Quat& q) {
    return Quat(
        -q.x,
        -q.y,
        -q.z,
        q.w
    );
}
// inverse
inline Quat Inverse(const Quat& q) {
    float magSqr = MagSqr(q);
    if (magSqr < 0.000001f) {
        return Quat();
    }
    /*float invMagSqr = 1.0f / magSqr;
    return Quat(
        -q.x * invMagSqr,
        -q.y * invMagSqr,
        -q.z * invMagSqr,
        q.w * invMagSqr
    );*/
    Quat res(
        vec_madd(
            q.vec,
            vec_splats(-1.0f/magSqr),
            (vec_float4){0.0f,0.0f,0.0f,0.0f}
        )
    );
    res.w = -res.w;
    return res;
}

// rotates v by q
// assumes q is normalized
inline Vec3 Transform(const Quat& q, const Vec3& v) {
    Vec3 u(q.x, q.y, q.z);
    return 
        (2.0f * Dot(u, v) * u +
        (q.w*q.w - Dot(u, u)) * v +
        2.0f * q.w * Cross(u, v));
}

struct Mat4
{
    // Column major ordering
    union
    {
        float v[4][4];
        struct { float r0c0, r1c0, r2c0, r3c0,
                       r0c1, r1c1, r2c1, r3c1,
                       r0c2, r1c2, r2c2, r3c2,
                       r0c3, r1c3, r2c3, r3c3; };
        struct { vec_float4 col0, col1, col2, col3; };
    };

    Mat4() :
        r0c0(1.0f), r1c0(0.0f), r2c0(0.0f), r3c0(0.0f),
        r0c1(0.0f), r1c1(1.0f), r2c1(0.0f), r3c1(0.0f),
        r0c2(0.0f), r1c2(0.0f), r2c2(1.0f), r3c2(0.0f),
        r0c3(0.0f), r1c3(0.0f), r2c3(0.0f), r3c3(1.0f)
    {
    }

    Mat4(const Mat4& other)/* :
        r0c0(other.r0c0), r1c0(other.r1c0), r2c0(other.r2c0), r3c0(other.r3c0),
        r0c1(other.r0c1), r1c1(other.r1c1), r2c1(other.r2c1), r3c1(other.r3c1),
        r0c2(other.r0c2), r1c2(other.r1c2), r2c2(other.r2c2), r3c2(other.r3c2),
        r0c3(other.r0c3), r1c3(other.r1c3), r2c3(other.r2c3), r3c3(other.r3c3)*/
    {
        col0 = other.col0;
        col1 = other.col1;
        col2 = other.col2;
        col3 = other.col3;
    }

    Mat4(float r0c0, float r0c1, float r0c2, float r0c3,
         float r1c0, float r1c1, float r1c2, float r1c3,
         float r2c0, float r2c1, float r2c2, float r2c3,
         float r3c0, float r3c1, float r3c2, float r3c3) :
        r0c0(r0c0), r1c0(r1c0), r2c0(r2c0), r3c0(r3c0),
        r0c1(r0c1), r1c1(r1c1), r2c1(r2c1), r3c1(r3c1),
        r0c2(r0c2), r1c2(r1c2), r2c2(r2c2), r3c2(r3c2),
        r0c3(r0c3), r1c3(r1c3), r2c3(r2c3), r3c3(r3c3)
    {}
    
    Mat4(vec_float4& col0,
         vec_float4& col1,
         vec_float4& col2,
         vec_float4& col3) :
        col0(col0),
        col1(col1),
        col2(col2),
        col3(col3)
    {}
    Mat4(vec_float4 col0,
         vec_float4 col1,
         vec_float4 col2,
         vec_float4 col3) :
        col0(col0),
        col1(col1),
        col2(col2),
        col3(col3)
    {}

    inline float operator()(int row, int col) const {
        return v[col][row];
    }

    Mat4& operator=(const Mat4& other) {
        /*r0c0 = other.r0c0;
        r0c1 = other.r0c1;
        r0c2 = other.r0c2;
        r0c3 = other.r0c3;
        
        r1c0 = other.r1c0;
        r1c1 = other.r1c1;
        r1c2 = other.r1c2;
        r1c3 = other.r1c3;
        
        r2c0 = other.r2c0;
        r2c1 = other.r2c1;
        r2c2 = other.r2c2;
        r2c3 = other.r2c3;
        
        r3c0 = other.r3c0;
        r3c1 = other.r3c1;
        r3c2 = other.r3c2;
        r3c3 = other.r3c3;*/
        col0 = other.col0;
        col1 = other.col1;
        col2 = other.col2;
        col3 = other.col3;
        
        return *this;
    }
    
    Mat4& operator+=(const Mat4& other) {
        /*r0c0 += other.r0c0;
        r0c1 += other.r0c1;
        r0c2 += other.r0c2;
        r0c3 += other.r0c3;
        
        r1c0 += other.r1c0;
        r1c1 += other.r1c1;
        r1c2 += other.r1c2;
        r1c3 += other.r1c3;
        
        r2c0 += other.r2c0;
        r2c1 += other.r2c1;
        r2c2 += other.r2c2;
        r2c3 += other.r2c3;
        
        r3c0 += other.r3c0;
        r3c1 += other.r3c1;
        r3c2 += other.r3c2;
        r3c3 += other.r3c3;*/
        col0 = vec_add(col0,other.col0);
        col1 = vec_add(col1,other.col1);
        col2 = vec_add(col2,other.col2);
        col3 = vec_add(col3,other.col3);
        
        return *this;
    }
};

inline Vec4 operator*(const Mat4& lhs, const Vec4& rhs) {
    //Vec4 res;
    vec_float4 tmp0, tmp1;
    vec_float4 xxxx, yyyy, zzzz, wwww;
    vec_float4 zero = (vec_float4){0.0f,0.0f,0.0f,0.0f};
    xxxx = vec_splat(rhs.vec,0);
    yyyy = vec_splat(rhs.vec,1);
    zzzz = vec_splat(rhs.vec,2);
    wwww = vec_splat(rhs.vec,3);
    tmp0 = vec_madd(lhs.col0,xxxx,zero);
    tmp1 = vec_madd(lhs.col1,yyyy,zero);
    tmp0 = vec_madd(lhs.col2,zzzz,tmp0);
    tmp1 = vec_madd(lhs.col3,wwww,tmp1);
    //res.vec = vec_add(tmp0,tmp1);
    //return res;
    return Vec4(vec_add(tmp0,tmp1));
}
inline Mat4 operator*(const Mat4& lhs, const Mat4& rhs) {
    Mat4 res;
    
    vec_float4 tmp0, tmp1;
    vec_float4 xxxx, yyyy, zzzz, wwww;
    vec_float4 zero = (vec_float4){0.0f,0.0f,0.0f,0.0f};
    xxxx = vec_splat(rhs.col0,0);
    yyyy = vec_splat(rhs.col0,1);
    zzzz = vec_splat(rhs.col0,2);
    wwww = vec_splat(rhs.col0,3);
    tmp0 = vec_madd(lhs.col0,xxxx,zero);
    tmp1 = vec_madd(lhs.col1,yyyy,zero);
    tmp0 = vec_madd(lhs.col2,zzzz,tmp0);
    tmp1 = vec_madd(lhs.col3,wwww,tmp1);
    res.col0 = vec_add(tmp0,tmp1);
    
    xxxx = vec_splat(rhs.col1,0);
    yyyy = vec_splat(rhs.col1,1);
    zzzz = vec_splat(rhs.col1,2);
    wwww = vec_splat(rhs.col1,3);
    tmp0 = vec_madd(lhs.col0,xxxx,zero);
    tmp1 = vec_madd(lhs.col1,yyyy,zero);
    tmp0 = vec_madd(lhs.col2,zzzz,tmp0);
    tmp1 = vec_madd(lhs.col3,wwww,tmp1);
    res.col1 = vec_add(tmp0,tmp1);
    
    xxxx = vec_splat(rhs.col2,0);
    yyyy = vec_splat(rhs.col2,1);
    zzzz = vec_splat(rhs.col2,2);
    wwww = vec_splat(rhs.col2,3);
    tmp0 = vec_madd(lhs.col0,xxxx,zero);
    tmp1 = vec_madd(lhs.col1,yyyy,zero);
    tmp0 = vec_madd(lhs.col2,zzzz,tmp0);
    tmp1 = vec_madd(lhs.col3,wwww,tmp1);
    res.col2 = vec_add(tmp0,tmp1);
    
    xxxx = vec_splat(rhs.col3,0);
    yyyy = vec_splat(rhs.col3,1);
    zzzz = vec_splat(rhs.col3,2);
    wwww = vec_splat(rhs.col3,3);
    tmp0 = vec_madd(lhs.col0,xxxx,zero);
    tmp1 = vec_madd(lhs.col1,yyyy,zero);
    tmp0 = vec_madd(lhs.col2,zzzz,tmp0);
    tmp1 = vec_madd(lhs.col3,wwww,tmp1);
    res.col3 = vec_add(tmp0,tmp1);
    
    return res;
}
inline Mat4 operator*(const Mat4& lhs, const float rhs) {
    /*return Mat4(
        lhs(0,0)*rhs, lhs(0,1)*rhs, lhs(0,2)*rhs, lhs(0,3)*rhs,
        lhs(1,0)*rhs, lhs(1,1)*rhs, lhs(1,2)*rhs, lhs(1,3)*rhs,
        lhs(2,0)*rhs, lhs(2,1)*rhs, lhs(2,2)*rhs, lhs(2,3)*rhs,
        lhs(3,0)*rhs, lhs(3,1)*rhs, lhs(3,2)*rhs, lhs(3,3)*rhs
    );*/
   /* Mat4 res;
    *((Vec4*)&res.col0) = (*((Vec4*)&lhs.col0)) * rhs;
    *((Vec4*)&res.col1) = (*((Vec4*)&lhs.col1)) * rhs;
    *((Vec4*)&res.col2) = (*((Vec4*)&lhs.col2)) * rhs;
    *((Vec4*)&res.col3) = (*((Vec4*)&lhs.col3)) * rhs;
    return res;*/
    //Mat4 res;
    vec_float4 ssss = vec_splats(rhs);
    vec_float4 zero = (vec_float4){0.0f,0.0f,0.0f,0.0f};
    //res.col0 = vec_madd(lhs.col0,ssss,zero);
    //res.col1 = vec_madd(lhs.col1,ssss,zero);
    //res.col2 = vec_madd(lhs.col2,ssss,zero);
    //res.col3 = vec_madd(lhs.col3,ssss,zero);
    //return res;
    return Mat4(
        vec_madd(lhs.col0,ssss,zero),
        vec_madd(lhs.col1,ssss,zero),
        vec_madd(lhs.col2,ssss,zero),
        vec_madd(lhs.col3,ssss,zero)
    );
}
inline Mat4 operator*(const float lhs, const Mat4& rhs) {
    return rhs * lhs;
}
inline Mat4 operator+(const Mat4& lhs, const Mat4& rhs) {
    /*return Mat4(
        lhs(0,0)+rhs(0,0), lhs(0,1)+rhs(0,1), lhs(0,2)+rhs(0,2), lhs(0,3)+rhs(0,3),
        lhs(1,0)+rhs(1,0), lhs(1,1)+rhs(1,1), lhs(1,2)+rhs(1,2), lhs(1,3)+rhs(1,3),
        lhs(2,0)+rhs(2,0), lhs(2,1)+rhs(2,1), lhs(2,2)+rhs(2,2), lhs(2,3)+rhs(2,3),
        lhs(3,0)+rhs(3,0), lhs(3,1)+rhs(3,1), lhs(3,2)+rhs(3,2), lhs(3,3)+rhs(3,3)
    );*/
    /*Mat4 result;
    result.col0 = vec_add(lhs.col0, rhs.col0);
    result.col1 = vec_add(lhs.col1, rhs.col1);
    result.col2 = vec_add(lhs.col2, rhs.col2);
    result.col3 = vec_add(lhs.col3, rhs.col3);
    return result;*/
    return Mat4(
        vec_add(lhs.col0,rhs.col0),
        vec_add(lhs.col1,rhs.col1),
        vec_add(lhs.col2,rhs.col2),
        vec_add(lhs.col3,rhs.col3)
    );
}

inline float Deg2Rad(const float deg) { return deg * M_PI / 180.0f; }
inline float Rad2Deg(const float rad) { return rad * 180.0f / M_PI; }
inline float Clamp(float val, const float min, float max) {
    return (val < min ? min : 
        (val > max ? max : val));
}

// multiplies v by m
inline Vec3 Transform(const Mat4& m, const Vec3& v, const float w = 1.0f) {
    /*return Vec3(
        m(0,0)*v.x + m(0,1)*v.y + m(0,2)*v.z + m(0,3)*w,
        m(1,0)*v.x + m(1,1)*v.y + m(1,2)*v.z + m(1,3)*w,
        m(2,0)*v.x + m(2,1)*v.y + m(2,2)*v.z + m(2,3)*w
    );*/
    return Vec3(Vec4(m * Vec4(v,w)));
}

inline Mat4 Translate(const Vec3& trans) {
    return Mat4(
        1.0f, 0.0f, 0.0f, trans.x,
        0.0f, 1.0f, 0.0f, trans.y,
        0.0f, 0.0f, 1.0f, trans.z,
        0.0f, 0.0f, 0.0f, 1.0f
    );
}
inline Mat4 Translate(float x, float y, float z) {
    return Mat4(
        1.0f, 0.0f, 0.0f, x,
        0.0f, 1.0f, 0.0f, y,
        0.0f, 0.0f, 1.0f, z,
        0.0f, 0.0f, 0.0f, 1.0f
    );
}

// https://github.com/ps3dev/PSL1GHT/blob/master/common/vectormath/ppu/cpp/mat_aos.h
// Matrix4::rotation()
inline Mat4 Rotate(const float angleRadians, const Vec3& axisIn) {
    /*const float c = cosf(angleRadians);
    const float s = sinf(angleRadians);
    
    const float axay = axis.x*axis.y;
    const float axaz = axis.x*axis.z;
    const float ayaz = axis.y*axis.z;
    const float ax2 = axis.x*axis.x;
    const float ay2 = axis.y*axis.y;
    const float az2 = axis.z*axis.z;

    return Mat4(
        c+(1-c)*ax2, (1-c)*axay-s*axis.z, (1-c)*axaz+s*axis.y, 0.0f,
        (1-c)*axay+s*axis.z, c+(1-c)*ay2, (1-c)*ayaz-s*axis.x, 0.0f,
        (1-c)*axaz-s*axis.y, (1-c)*ayaz+s*axis.x, c+(1-c)*az2, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    );*/
    vec_float4 axis, s, c, oneMinusC, axisS, negAxisS, xxxx, yyyy, zzzz, tmp0, tmp1, tmp2, zeroW;
    vec_float4 zero = ((vec_float4){0.0f,0.0f,0.0f,0.0f});
    axis = (vec_float4){axisIn.x,axisIn.y,axisIn.z,0.0f};
    sincosf4( vec_splats(angleRadians), &s, &c );
    xxxx = vec_splat( axis, 0 );
    yyyy = vec_splat( axis, 1 );
    zzzz = vec_splat( axis, 2 );
    oneMinusC = vec_sub( ((vec_float4){1.0f,1.0f,1.0f,1.0f}), c );
    axisS = vec_madd( axis, s, zero );
    negAxisS = negatef4( axisS );
    tmp0 = vec_perm( axisS, negAxisS, _VECTORMATH_PERM_XZBX );
    tmp1 = vec_perm( axisS, negAxisS, _VECTORMATH_PERM_CXXX );
    tmp2 = vec_perm( axisS, negAxisS, _VECTORMATH_PERM_YAXX );
    tmp0 = vec_sel( tmp0, c, _VECTORMATH_MASK_0xF000 );
    tmp1 = vec_sel( tmp1, c, _VECTORMATH_MASK_0x0F00 );
    tmp2 = vec_sel( tmp2, c, _VECTORMATH_MASK_0x00F0 );
    zeroW = (vec_float4)_VECTORMATH_MASK_0x000F;
    axis = vec_andc( axis, zeroW );
    tmp0 = vec_andc( tmp0, zeroW );
    tmp1 = vec_andc( tmp1, zeroW );
    tmp2 = vec_andc( tmp2, zeroW );
    return Mat4(
        vec_madd( vec_madd( axis, xxxx, zero ), oneMinusC, tmp0 ),
        vec_madd( vec_madd( axis, yyyy, zero ), oneMinusC, tmp1 ),
        vec_madd( vec_madd( axis, zzzz, zero ), oneMinusC, tmp2 ),
        _VECTORMATH_UNIT_0001
    );
}

inline Mat4 Scale(const Vec3& s) {
    return Mat4(
        s.x, 0.0f, 0.0f, 0.0f,
        0.0f, s.y, 0.0f, 0.0f,
        0.0f, 0.0f, s.z, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    );
}
inline Mat4 Scale(float x, float y, float z) {
    return Mat4(
        x, 0.0f, 0.0f, 0.0f,
        0.0f, y, 0.0f, 0.0f,
        0.0f, 0.0f, z, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    );
}

// https://github.com/ps3dev/PSL1GHT/blob/master/common/vectormath/ppu/cpp/mat_aos.h
// Matrix3 quat constructor
inline Mat4 QuatToMat4(const Quat& q) {
    Mat4 res;
    vec_float4 xyzw_2, wwww, yzxw, zxyw, yzxw_2, zxyw_2;
    vec_float4 tmp0, tmp1, tmp2, tmp3, tmp4, tmp5;
    vec_uint4 select_x = _VECTORMATH_MASK_0xF000;
    vec_uint4 select_z = _VECTORMATH_MASK_0x00F0;
    xyzw_2 = vec_add( q.vec, q.vec );
    wwww = vec_splat( q.vec, 3 );
    yzxw = vec_perm( q.vec, q.vec, _VECTORMATH_PERM_YZXW );
    zxyw = vec_perm( q.vec, q.vec, _VECTORMATH_PERM_ZXYW );
    yzxw_2 = vec_perm( xyzw_2, xyzw_2, _VECTORMATH_PERM_YZXW );
    zxyw_2 = vec_perm( xyzw_2, xyzw_2, _VECTORMATH_PERM_ZXYW );
    tmp0 = vec_madd( yzxw_2, wwww, ((vec_float4){0.0f,0.0f,0.0f,0.0f}) );
    tmp1 = vec_nmsub( yzxw, yzxw_2, ((vec_float4){1.0f,1.0f,1.0f,1.0f}) );
    tmp2 = vec_madd( yzxw, xyzw_2, ((vec_float4){0.0f,0.0f,0.0f,0.0f}) );
    tmp0 = vec_madd( zxyw, xyzw_2, tmp0 );
    tmp1 = vec_nmsub( zxyw, zxyw_2, tmp1 );
    tmp2 = vec_nmsub( zxyw_2, wwww, tmp2 );
    tmp3 = vec_sel( tmp0, tmp1, select_x );
    tmp4 = vec_sel( tmp1, tmp2, select_x );
    tmp5 = vec_sel( tmp2, tmp0, select_x );
    res.col0 = vec_sel( tmp3, tmp2, select_z );
    res.col1 = vec_sel( tmp4, tmp0, select_z );
    res.col2 = vec_sel( tmp5, tmp1, select_z );
    res.col3 = _VECTORMATH_UNIT_0001;
    // last row should be 0,0,0,1
    res.r3c0 = res.r3c1 = res.r3c2 = 0.0f;
    return res;
}

// rotation upon "from" in order to get to "to"
inline Quat FromTo(const Vec3& from, const Vec3& to) {
    Vec3 f = Normalize(from);
    Vec3 t = Normalize(to);
    if (f == t) {
        return Quat();
    }
    // opposition directions
    else if (f == (t * -1.0f)) {
        // the most orthogonal axis of from used to create quat
        Vec3 ortho = Vec3(1,0,0);
        if (fabsf(f.y) < fabsf(f.x)) {
            ortho = Vec3(0,1,0);
        }
        if (fabsf(f.z) < fabsf(f.y) &&
            fabsf(f.z) < fabsf(f.x))
        {
            ortho = Vec3(0,0,1);
        }
        
        Vec3 axis = Normalize(Cross(f, ortho));
        return Quat(axis.x, axis.y, axis.z, 0);
    }
    
    // sin of the half vector between the two (cross product) is the
    // axis of rotation, and cos of the half vector (dot product) is the angle
    Vec3 half = Normalize(f + t);
    Vec3 axis = Cross(f, half);
    return Quat(axis.x, axis.y, axis.z, Dot(f, half)); // dot == angle
}

// rotation for this direction
inline Quat LookRotation(const Vec3& direction, const Vec3& up) {
    // find orthonormal basis vectors
    Vec3 f = Normalize(direction); // forward
    Vec3 u = Normalize(up); // desired up
    Vec3 r = Cross(u, f); // right
    u = Cross(f, r); // object up
    
    // from world forward to object forward
    Quat worldToObject = FromTo(Vec3(0,0,1), f);
    
    // new object up direction
    Vec3 objectUp = GameMath::Transform(worldToObject, Vec3(0,1,0));
    
    // new object up to desired up
    Quat u2u = FromTo(objectUp, u);
    
    // rotate forward direction then twist up
    Quat result = worldToObject * u2u;
    return Normalize(result);
}

inline Quat Mat4ToQuat(const Mat4& m) {
    // mat 2nd col = up
    Vec3 up(m.r0c1, m.r1c1, m.r2c1);
    up = GameMath::Normalize(up);
    
    // mat 3rd col = forward
    Vec3 forward(m.r0c2, m.r1c2, m.r2c2);
    forward = GameMath::Normalize(forward);
    
    Vec3 right = GameMath::Cross(up, forward);
    up = GameMath::Cross(forward, right);
    
    return LookRotation(forward, up);
}

// Foundations of Game Engine Development Vol. 1 pg. 49
inline Mat4 Inverse(const Mat4& m) {
    Vec3 a(m(0,0), m(1,0), m(2,0));
    Vec3 b(m(0,1), m(1,1), m(2,1));
    Vec3 c(m(0,2), m(1,2), m(2,2));
    Vec3 d(m(0,3), m(1,3), m(2,3));
    
    const float x = m(3,0);
    const float y = m(3,1);
    const float z = m(3,2);
    const float w = m(3,3);
    
    Vec3 s = Cross(a, b);
    Vec3 t = Cross(c, d);
    Vec3 u = y*a - x*b;
    Vec3 v = w*c - z*d;
    
    float detInv = 1.0f / (Dot(s, v) + Dot(t, u));
    Vec3 row0 = (Cross(b, v) + y*t);
    Vec3 row1 = (Cross(v, a) - x*t);
    Vec3 row2 = (Cross(d, u) + w*s);
    Vec3 row3 = (Cross(u, c) - z*s);
    
    return Mat4(
        row0.x, row0.y, row0.z, -Dot(b, t),
        row1.x, row1.y, row1.z, Dot(a, t),
        row2.x, row2.y, row2.z, -Dot(d, s),
        row3.x, row3.y, row3.z, Dot(c, s)
    ) * detInv;
}

inline Mat4 Transpose(const Mat4& m) {
    return Mat4(
        m(0,0), m(1,0), m(2,0), m(3,0),
        m(0,1), m(1,1), m(2,1), m(3,1),
        m(0,2), m(1,2), m(2,2), m(3,2),
        m(0,3), m(1,3), m(2,3), m(3,3)
    );
}

// https://www.geertarien.com/blog/2017/07/30/breakdown-of-the-lookAt-function-in-OpenGL/
inline Mat4 LookAt(const Vec3& eye, const Vec3& at, const Vec3& up) {
    Vec3 zaxis = Normalize(at - eye);
    Vec3 xaxis = Normalize(Cross(zaxis, up));
    Vec3 yaxis = Cross(xaxis, zaxis);
    
    zaxis = zaxis * -1.0f;
    
    return Mat4(
        xaxis.x, xaxis.y, xaxis.z, -Dot(xaxis, eye),
        yaxis.x, yaxis.y, yaxis.z, -Dot(yaxis, eye),
        zaxis.x, zaxis.y, zaxis.z, -Dot(zaxis, eye),
        0.0f, 0.0f, 0.0f, 1.0f
    );
}

// https://github.com/ps3dev/PSL1GHT/blob/master/common/vectormath/ppu/cpp/mat_aos.h
inline Mat4 Perspective(const float fovRad, const float aspect, const float near, const float far) {
    /*Mat4 result;
    float f, rangeInv;
    Vec4 zero, col0, col1, col2, col3;
    Vec4 tmp;
    f = tanf((M_PI/2.0f) - fovRad * 0.5f);
    rangeInv = 1.0f / (near - far);
    zero = Vec4(0.0f, 0.0f, 0.0f, 0.0f);
    tmp = zero;
    tmp.v[0] = f / aspect;
    col0 = tmp;
    tmp = zero;
    tmp.v[1] = f;
    col1 = tmp;
    tmp = zero;
    tmp.v[2] = (near + far) * rangeInv;
    tmp.v[3] = -1.0f;
    col2 = tmp;
    tmp = zero;
    tmp.v[2] = near * far * rangeInv * 2.0f;
    col3 = tmp;
    return Mat4(
        col0.x, col1.x, col2.x, col3.x,
        col0.y, col1.y, col2.y, col3.y,
        col0.z, col1.z, col2.z, col3.z,
        col0.w, col1.w, col2.w, col3.w
    );*/
    Mat4 res;
    float f, rangeInv;
    vec_float4 zero = (vec_float4){0.0f,0.0f,0.0f,0.0f};
    f = tanf((M_PI/2.0f) - fovRad * 0.5f);
    rangeInv = 1.0f / (near - far);
    res.col0 = zero;
    res.r0c0 = f / aspect;
    res.col1 = zero;
    res.r1c1 = f;
    res.col2 = zero;
    res.r2c2 = (near + far) * rangeInv;
    res.r3c2 = -1.0f;
    res.col3 = zero;
    res.r2c3 = near * far * rangeInv * 2.0f;
    return res;
}

// adapted from raylib rcore.c
inline Vec2 WorldToScreenCoords(Vec3& pos, Mat4& viewMat, Mat4& projMat, int scrWidth, int scrHeight) {
    Mat4 viewProj = projMat * viewMat;
    // we want to calculate and include the w coord
    Vec4 worldPos = viewProj * Vec4(pos, 1.0f);
    float w = worldPos.w;
    if (fabsf(w) > 0.00001f) {
        w = 1.0f / w;
    }
    // normalized device coordinates (inverted y)
    Vec3 ndcPos(worldPos.x * w, -worldPos.y * w, worldPos.z * w);
    // 2d screen pos
    return Vec2(
        (ndcPos.x + 1.0f) / 2.0f * ((float)scrWidth),
        (ndcPos.y + 1.0f) / 2.0f * ((float)scrHeight)
    );
}

} // end namespace GameMath

#endif // GAMEMATH_CPP_H_INCLUDED

