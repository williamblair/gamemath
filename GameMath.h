#ifndef GAMEMATH_IMPL_H_INCLUDED
#define GAMEMATH_IMPL_H_INCLUDED

#ifdef PS3_BUILD
#include "ps3/GameMath.h"
//#include "cpp/GameMath.h"
#endif

#ifdef WII_BUILD
#include "wii/GameMath.h"
#endif

#ifdef PC_BUILD
#include "cpp/GameMath.h"
#endif

#ifdef PSP_BUILD
#include "psp/GameMath.h"
//#include "cpp/GameMath.h"
#endif

#endif // GAMEMATH_IMPL_H_INCLUDED

