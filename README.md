# GameMath

Header only Vector/Matrix math

Requires one of the following to be defined during the build process:

```
PS3_BUILD
WII_BUILD
PC_BUILD
```

As in, for gcc,
```
-DPC_BUILD
```

Then include via
```
#include <GameMath/GameMath.h>
```

